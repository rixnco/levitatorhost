package com.rxnco.levitatorhost;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.DynamicTimeSeriesCollection;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.ui.ExtensionFileFilter;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JCheckBox;

public class Main implements SerialPortEventListener {
	static private final String VERSION;

	protected static final int REPORT_PERIOD = 150;
	
	static {
		String v=null; 
		Properties props= new Properties();
		try {
			props.load(Main.class.getResourceAsStream("/build.properties"));
			v=props.getProperty("VERSION");
		} catch (Throwable th) {
		}
		VERSION=v==null?"":v;
	}
	
	
	private JFrame frame;

	private Timer timer = new Timer();
	private TimerTask task;
	private TimerTask watchdog;
	private long lastReportDate;
	
	private DynamicTimeSeriesCollection sensorDataset;
	private DynamicTimeSeriesCollection pwmDataset;
	private JToggleButton tglbtnConnect;

	private SerialPort serialPort;

	private XYPlot plot;
	private NumberAxis sensorAxis;
	private NumberAxis coilAxis;

	private boolean connected;

	private float[] sensorData = new float[2];
	private float[] pwmData = new float[1];
	private byte[] buffer = new byte[200];
	private int received = 0;
	private boolean readA = false;
	private boolean readB = false;
	private boolean readSettings = false;

	private JPanel serialPanel;
	private JLabel lblSetpoint;
	private JSpinner spSpinnerA;
	private JLabel lblKP;
	private JSpinner kpSpinnerA;
	private JLabel lblKI;
	private JSpinner kiSpinnerA;
	private JLabel lblKD;
	private JSpinner kdSpinnerA;
	private JButton btnSendA;
	private JButton btnGenerate;
	private JLabel lblA;
	private JLabel lblB;
	private JButton btnSendB;
	private JButton btnReadA;
	private JButton btnReadB;
	private JSpinner spSpinnerB;
	private JSpinner kpSpinnerB;
	private JSpinner kdSpinnerB;
	private JSpinner kiSpinnerB;
	private JTextField spTextField;
	private JTextField kpTextField;
	private JTextField kdTextField;
	private JTextField kiTextField;
	@SuppressWarnings("rawtypes")
	private JComboBox waveFormCombo;
	private JSpinner wavePeriodSpinner;
	private JSpinner waveStepsSpinner;
	private JLabel lblGenerator;
	private JLabel lblPeriod;
	private JLabel lblSteps;
	@SuppressWarnings("rawtypes")
	private JComboBox serialPortCombo;
	private JButton btnRefresh;

	private JLabel lblCtrlPeriod;
	private JSpinner ctrlPeriodSpinner;
	private JLabel lblSensFilter;
	private JSpinner sensFilterSpinner;
	private JLabel lblCtrlTimeout;
	private JSpinner actvTimeoutSpinner;
	private JLabel lblThreshold;
	private JSpinner actvThresholdSpinner;
	private JPanel controllerPanel;
	private JLabel lblCoilFilter;
	private JSpinner coilFilterSpinner;
	private JLabel lblSetPoint;
	private JLabel lblKp;
	private JLabel lblKd;
	private JLabel lblKi;
	private JButton btnSaveDefault;
	private JButton btnRestoreDefault;
	private JPanel rightPane;
	private JPanel panel;
	private JPanel leftPanel;
	private JLabel lblCoilMax;
	private JSpinner coilAxisMinSpinner;
	private JSpinner coilAxisMaxSpinner;
	private JSpinner sensorAxisMinSpinner;
	private JSpinner sensorAxisMaxSpinner;
	private JPanel axisPanel;
	private JPanel coilAxisPanel;
	private JPanel bottomPanel;
	private JPanel settingsPannel;
	private JLabel lblCtrlPeriod_1;
	private JLabel lblThreshold_1;
	private JLabel lblTimeout;
	private JLabel lblSensFilter_1;
	private JLabel lblCoilFIlter_1;
	private JTextField ctrlPeriodTextField;
	private JTextField thresholdTextField;
	private JTextField ctrlTimeoutTextField;
	private JTextField sensFilterTextField;
	private JTextField coilFilterTextField;
	private JButton btnSettingsSend;
	private JButton btnSettingsRead;
	private Component horizontalGlue_1;
	private JLabel lblCoilMin_1;
	private JTextField coilMinTextField;
	private JTextField coilMidTextField;
	private JTextField coilMaxTextField;
	private JSpinner coilMinSpinner;
	private JSpinner coilMidSpinner;
	private JSpinner coilMaxSpinner;

	private boolean modified = false;
	private File configFile = null;
	private final JFileChooser fc = new JFileChooser();

	private Properties config= new Properties();

	protected float spValue;
	protected float kpValue;
	protected float kdValue;
	protected float kiValue;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Locale.setDefault(Locale.US);

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
		refreshPorts();
		loadDefaults();
	}

	private void loadDefaults() {
		String user_home= System.getProperty("user.home");
		File levitatorDir= new File(user_home, ".levitator");
		try {
			config.load(new FileInputStream(new File(levitatorDir, "defaults.cfg")));

			spSpinnerA.setValue(getInteger(config, "SPA", 0));
			kpSpinnerA.setValue(getDouble(config, "KPA", 0));
			kdSpinnerA.setValue(getDouble(config, "KDA", 0));
			kiSpinnerA.setValue(getDouble(config, "KIA", 0));

			spSpinnerB.setValue(getInteger(config, "SPB", 0));
			kpSpinnerB.setValue(getDouble(config, "KPB", 0));
			kdSpinnerB.setValue(getDouble(config, "KDB", 0));
			kiSpinnerB.setValue(getDouble(config, "KIB", 0));

			waveFormCombo.setSelectedItem(getString(config, "WAVEFORM", "Sin"));
			wavePeriodSpinner.setValue(getInteger(config, "WAVEPERIOD", 3000));
			waveStepsSpinner.setValue(getInteger(config, "WAVESTEPS", 100));

			ctrlPeriodSpinner.setValue(getInteger(config, "CTRLPERIOD", 1));
			actvThresholdSpinner.setValue(getInteger(config, "ACTVTHRESHOLD", 400));
			actvTimeoutSpinner.setValue(getInteger(config, "ACTVTIMEOUT", 2000));

			sensorReversedCheckBox.setSelected(getInteger(config, "SENSRVSD", 0)!=0?true:false);
			sensFilterSpinner.setValue(getInteger(config, "SENSFILTER", 3));
			coilFilterSpinner.setValue(getInteger(config, "COILFILTER", 1));

			coilMinSpinner.setValue(getInteger(config, "COILMIN", 0));
			coilMidSpinner.setValue(getInteger(config, "COILMID", 50));
			coilMaxSpinner.setValue(getInteger(config, "COILMAX", 100));
			
			// HMI - Applicaation
			
			chckbxmntmLoadSettingsUpon.setSelected(Boolean.parseBoolean(config.getProperty("LOAD_SETTINGS")));
			chckbxmntmLoadPidA.setSelected(Boolean.parseBoolean(config.getProperty("LOAD_PID_A")));
			chckbxmntmLoadPidB.setSelected(Boolean.parseBoolean(config.getProperty("LOAD_PID_B")));

			coilAxisMinSpinner.setValue(getInteger(config, "COILAXISMIN", 0));
			coilAxisMaxSpinner.setValue(getInteger(config, "COILAXISMAX", 100));

			sensorAxisMinSpinner.setValue(getInteger(config, "SENSAXISMIN", 0));
			sensorAxisMaxSpinner.setValue(getInteger(config, "SENSAXISMAX", 1024));
			
			serialPortCombo.setSelectedItem(config.getProperty("COMPORT", ""));
			
			
			String currentDir= config.getProperty("CURRENT_DIR");
			if(currentDir!=null) {
				File dir= new File(currentDir);
				if(dir.exists()) fc.setCurrentDirectory(dir);
			}
			
		} catch(IOException ex) {
			System.out.println("Unable to read settings. Using default values");
		}
		setModified(false);
	}

	private void saveAsDefaults() {
		config.setProperty("LOAD_SETTINGS", Boolean.toString(chckbxmntmLoadSettingsUpon.isSelected()));
		config.setProperty("LOAD_PID_A", Boolean.toString(chckbxmntmLoadPidA.isSelected()));
		config.setProperty("LOAD_PID_B", Boolean.toString(chckbxmntmLoadPidB.isSelected()));

		config.setProperty("SPA", String.format("%d", getSPA()));
		config.setProperty("KPA", String.format("%.2f", getKPA()));
		config.setProperty("KDA", String.format("%.2f", getKDA()));
		config.setProperty("KIA", String.format("%.4f", getKIA()));
		config.setProperty("SPB", String.format("%d", getSPB()));
		config.setProperty("KPB", String.format("%.2f", getKPB()));
		config.setProperty("KDB", String.format("%.2f", getKDB()));
		config.setProperty("KIB", String.format("%.4f", getKIB()));
		config.setProperty("WAVEFORM", getWaveForm());
		config.setProperty("WAVEPERIOD", String.format("%d", getWavePeriod()));
		config.setProperty("WAVESTEPS", String.format("%d", getWaveSteps()));

		config.setProperty("CTRLPERIOD", String.format("%d", getCtrlPeriod()));
		config.setProperty("ACTVTHRESHOLD", String.format("%d", getActvThreshold()));
		config.setProperty("ACTVTIMEOUT", String.format("%d", getActvTimeout()));
		config.setProperty("SENSRVSD", String.format("%d", getSensReversed()?1:0));
		config.setProperty("SENSFILTER", String.format("%d", getSensFilter()));
		config.setProperty("COILFILTER", String.format("%d", getCoilFilter()));
		config.setProperty("COILMIN", String.format("%d", getCoilMin()));
		config.setProperty("COILMID", String.format("%d", getCoilMid()));
		config.setProperty("COILMAX", String.format("%d", getCoilMax()));
		
		config.setProperty("COILAXISMIN", String.format("%d",getCoilAxisMin()));
		config.setProperty("COILAXISMAX", String.format("%d",getCoilAxisMax()));
		
		config.setProperty("SENSAXISMIN", String.format("%d",getSensAxisMin()));
		config.setProperty("SENSAXISMAX", String.format("%d",getSensAxisMax()));
		
		saveConfig();
		if(configFile==null) setModified(false);
	}
	
	private void saveConfig() {
		String user_home= System.getProperty("user.home");
		File levitatorDir= new File(user_home, ".levitator");
		levitatorDir.mkdirs();
		try {
			config.setProperty("VERSION", "1.1");
			
			config.setProperty("COMPORT", serialPortCombo.getSelectedItem()==null?"":serialPortCombo.getSelectedItem().toString());
			
			if(fc.getCurrentDirectory()!=null) {
				config.setProperty("CURRENT_DIR", fc.getCurrentDirectory().getAbsolutePath());
			}
			
			config.store(new FileOutputStream(new File(levitatorDir, "defaults.cfg")), "");
			
		} catch(IOException ex) {
			System.out.println("Failed to store configuration");
		}
	}
	
	
	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize() {
		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				doExit();
			}
		});
		frame.setTitle(getTitle());
		frame.setBounds(100, 100, 1600, 1024);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		sensorDataset = new MyDynamicTimeSeriesCollection(2, 100, new MultipleOfMillisecond(REPORT_PERIOD));
		sensorDataset.setTimeBase(new MultipleOfMillisecond(REPORT_PERIOD));
		pwmDataset = new MyDynamicTimeSeriesCollection(1, 100, new MultipleOfMillisecond(REPORT_PERIOD));
		pwmDataset.setTimeBase(new MultipleOfMillisecond(REPORT_PERIOD));

		plot = new XYPlot();
		plot.setDataset(0, sensorDataset);
		plot.setDataset(1, pwmDataset);

		XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer(true, false);
		renderer1.setSeriesPaint(0, Color.BLUE);
		renderer1.setSeriesPaint(1, Color.RED);
		plot.setRenderer(0, renderer1);

		XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer(true, false);
		renderer2.setSeriesPaint(0, Color.GREEN);
		plot.setRenderer(1, renderer2);

		// Time base axis (domain)
		ValueAxis axis = new DateAxis("Time");
		axis.setAutoRange(true);
		axis.setVisible(true);
		axis.setLowerMargin(0.02); // reduce the default margins
		axis.setUpperMargin(0.02);
		plot.setDomainAxis(axis);
		// axis.setMinorTickCount(10);
		// axis.setMinorTickMarksVisible(true);

		plot.setDomainGridlinesVisible(true);
		// plot.setDomainMinorGridlinesVisible(true);
		// plot.setDomainMinorGridlinePaint(Color.LIGHT_GRAY);

		// Sensor Axis (range)
		sensorAxis = new NumberAxis("Sensor");
		sensorAxis.setRange(0, 1024);
		plot.setRangeAxis(1, sensorAxis);

		// PWM Axis
		coilAxis = new NumberAxis("Coil");
		coilAxis.setRange(0, 100);
		plot.setRangeAxis(0, coilAxis);

		plot.mapDatasetToRangeAxis(1, 0);
		plot.mapDatasetToRangeAxis(0, 1);

		JFreeChart chart = new JFreeChart(null, JFreeChart.DEFAULT_TITLE_FONT, plot, false);
		sensorDataset.addSeries(new float[] { 0f }, 0, "S1");
		sensorDataset.addSeries(new float[] { 0f }, 1, "S2");
		pwmDataset.addSeries(new float[] { 0f }, 0, "S3");

		rightPane = new JPanel();
		frame.getContentPane().add(rightPane, BorderLayout.CENTER);
		rightPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		rightPane.setLayout(new BorderLayout());

		ChartPanel plotPanel = new ChartPanel(chart);
		rightPane.add(plotPanel, BorderLayout.CENTER);

		bottomPanel = new JPanel();
		rightPane.add(bottomPanel, BorderLayout.SOUTH);
		GridBagLayout gbl_bottomPanel = new GridBagLayout();
		gbl_bottomPanel.columnWeights = new double[] { 0.0, 0.0 };
		gbl_bottomPanel.rowWeights = new double[] { 0.0 };
		bottomPanel.setLayout(gbl_bottomPanel);

		JPanel PIDPanel = new JPanel();
		GridBagConstraints gbc_PIDPanel = new GridBagConstraints();
		gbc_PIDPanel.anchor = GridBagConstraints.EAST;
		gbc_PIDPanel.weightx = 1.0;
		gbc_PIDPanel.insets = new Insets(0, 0, 0, 5);
		gbc_PIDPanel.gridx = 0;
		gbc_PIDPanel.gridy = 0;
		bottomPanel.add(PIDPanel, gbc_PIDPanel);
		PIDPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "PID", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagLayout gbl_PIDPanel = new GridBagLayout();
		gbl_PIDPanel.columnWidths = new int[] { 20, 100, 100, 100, 100, 50, 50, 30 };
		gbl_PIDPanel.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		PIDPanel.setLayout(gbl_PIDPanel);

		lblSetpoint = new JLabel("SetPoint");
		GridBagConstraints gbc_lblSetpoint = new GridBagConstraints();
		gbc_lblSetpoint.insets = new Insets(0, 0, 5, 5);
		gbc_lblSetpoint.gridx = 1;
		gbc_lblSetpoint.gridy = 0;
		PIDPanel.add(lblSetpoint, gbc_lblSetpoint);

		lblKP = new JLabel("P");
		GridBagConstraints gbc_lblKP = new GridBagConstraints();
		gbc_lblKP.insets = new Insets(0, 0, 5, 5);
		gbc_lblKP.gridx = 2;
		gbc_lblKP.gridy = 0;
		PIDPanel.add(lblKP, gbc_lblKP);

		lblKD = new JLabel("D");
		GridBagConstraints gbc_lblKD = new GridBagConstraints();
		gbc_lblKD.insets = new Insets(0, 0, 5, 5);
		gbc_lblKD.gridx = 3;
		gbc_lblKD.gridy = 0;
		PIDPanel.add(lblKD, gbc_lblKD);

		lblKI = new JLabel("I");
		GridBagConstraints gbc_lblKI = new GridBagConstraints();
		gbc_lblKI.insets = new Insets(0, 0, 5, 5);
		gbc_lblKI.gridx = 4;
		gbc_lblKI.gridy = 0;
		PIDPanel.add(lblKI, gbc_lblKI);

		lblA = new JLabel("A");
		GridBagConstraints gbc_lblA = new GridBagConstraints();
		gbc_lblA.insets = new Insets(0, 0, 5, 5);
		gbc_lblA.gridx = 0;
		gbc_lblA.gridy = 1;
		PIDPanel.add(lblA, gbc_lblA);

		spSpinnerA = new JSpinner();
		spSpinnerA.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
		spSpinnerA.setModel(new SpinnerNumberModel(270, 0, 1000, 1));
		spSpinnerA.setEditor(new JSpinner.NumberEditor(spSpinnerA, "#"));
		GridBagConstraints gbc_spSpinnerA = new GridBagConstraints();
		gbc_spSpinnerA.insets = new Insets(0, 0, 5, 5);
		gbc_spSpinnerA.fill = GridBagConstraints.HORIZONTAL;
		gbc_spSpinnerA.gridx = 1;
		gbc_spSpinnerA.gridy = 1;
		PIDPanel.add(spSpinnerA, gbc_spSpinnerA);

		kpSpinnerA = new JSpinner();
		kpSpinnerA.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
		kpSpinnerA.setModel(new SpinnerNumberModel(new Double(1.75), null, null, new Double(0.1)));
		kpSpinnerA.setEditor(new JSpinner.NumberEditor(kpSpinnerA, "0.00"));
		GridBagConstraints gbc_kpSpinnerA = new GridBagConstraints();
		gbc_kpSpinnerA.fill = GridBagConstraints.HORIZONTAL;
		gbc_kpSpinnerA.insets = new Insets(0, 0, 5, 5);
		gbc_kpSpinnerA.gridx = 2;
		gbc_kpSpinnerA.gridy = 1;
		PIDPanel.add(kpSpinnerA, gbc_kpSpinnerA);

		kdSpinnerA = new JSpinner();
		kdSpinnerA.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
		kdSpinnerA.setModel(new SpinnerNumberModel(new Double(24.0), null, null, new Double(0.1)));
		kdSpinnerA.setEditor(new JSpinner.NumberEditor(kdSpinnerA, "0.00"));
		GridBagConstraints gbc_kdSpinnerA = new GridBagConstraints();
		gbc_kdSpinnerA.fill = GridBagConstraints.HORIZONTAL;
		gbc_kdSpinnerA.insets = new Insets(0, 0, 5, 5);
		gbc_kdSpinnerA.gridx = 3;
		gbc_kdSpinnerA.gridy = 1;
		PIDPanel.add(kdSpinnerA, gbc_kdSpinnerA);

		kiSpinnerA = new JSpinner();
		kiSpinnerA.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
		kiSpinnerA.setModel(new SpinnerNumberModel(new Double(0.0033), null, null, new Double(0.0001)));
		kiSpinnerA.setEditor(new JSpinner.NumberEditor(kiSpinnerA, "0.0000"));
		GridBagConstraints gbc_kiSpinnerA = new GridBagConstraints();
		gbc_kiSpinnerA.fill = GridBagConstraints.HORIZONTAL;
		gbc_kiSpinnerA.insets = new Insets(0, 0, 5, 5);
		gbc_kiSpinnerA.gridx = 4;
		gbc_kiSpinnerA.gridy = 1;
		PIDPanel.add(kiSpinnerA, gbc_kiSpinnerA);

		btnSendA = new JButton("send");
		btnSendA.setEnabled(false);
		btnSendA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendParameters(getSPA(), getKPA(), getKIA(), getKDA());
				query();
			}
		});
		GridBagConstraints gbc_btnSendA = new GridBagConstraints();
		gbc_btnSendA.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSendA.anchor = GridBagConstraints.EAST;
		gbc_btnSendA.insets = new Insets(0, 0, 5, 5);
		gbc_btnSendA.gridx = 5;
		gbc_btnSendA.gridy = 1;
		PIDPanel.add(btnSendA, gbc_btnSendA);

		btnReadA = new JButton("read");
		btnReadA.setEnabled(false);
		btnReadA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				readA = true;
				query();

			}
		});
		GridBagConstraints gbc_btnReadA = new GridBagConstraints();
		gbc_btnReadA.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnReadA.insets = new Insets(0, 0, 5, 5);
		gbc_btnReadA.gridx = 6;
		gbc_btnReadA.gridy = 1;
		PIDPanel.add(btnReadA, gbc_btnReadA);

		Component horizontalGlue = Box.createHorizontalGlue();
		GridBagConstraints gbc_horizontalGlue = new GridBagConstraints();
		gbc_horizontalGlue.weightx = 10.0;
		gbc_horizontalGlue.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalGlue.gridx = 7;
		gbc_horizontalGlue.gridy = 1;
		PIDPanel.add(horizontalGlue, gbc_horizontalGlue);

		lblB = new JLabel("B");
		GridBagConstraints gbc_lblB = new GridBagConstraints();
		gbc_lblB.insets = new Insets(0, 0, 5, 5);
		gbc_lblB.gridx = 0;
		gbc_lblB.gridy = 2;
		PIDPanel.add(lblB, gbc_lblB);

		spSpinnerB = new JSpinner();
		spSpinnerB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
		spSpinnerB.setModel(new SpinnerNumberModel(315, 0, 1000, 1));
		spSpinnerB.setEditor(new JSpinner.NumberEditor(spSpinnerB, "#"));
		GridBagConstraints gbc_spSpinnerB = new GridBagConstraints();
		gbc_spSpinnerB.fill = GridBagConstraints.HORIZONTAL;
		gbc_spSpinnerB.insets = new Insets(0, 0, 5, 5);
		gbc_spSpinnerB.gridx = 1;
		gbc_spSpinnerB.gridy = 2;
		PIDPanel.add(spSpinnerB, gbc_spSpinnerB);

		kpSpinnerB = new JSpinner();
		kpSpinnerB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
		kpSpinnerB.setModel(new SpinnerNumberModel(new Double(2.1), null, null, new Double(0.1)));
		kpSpinnerB.setEditor(new JSpinner.NumberEditor(kpSpinnerB, "0.00"));

		GridBagConstraints gbc_kpSpinnerB = new GridBagConstraints();
		gbc_kpSpinnerB.fill = GridBagConstraints.HORIZONTAL;
		gbc_kpSpinnerB.insets = new Insets(0, 0, 5, 5);
		gbc_kpSpinnerB.gridx = 2;
		gbc_kpSpinnerB.gridy = 2;
		PIDPanel.add(kpSpinnerB, gbc_kpSpinnerB);

		kdSpinnerB = new JSpinner();
		kdSpinnerB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
		kdSpinnerB.setModel(new SpinnerNumberModel(new Double(27.0), null, null, new Double(0.1)));
		kdSpinnerB.setEditor(new JSpinner.NumberEditor(kdSpinnerB, "0.00"));
		GridBagConstraints gbc_kdSpinnerB = new GridBagConstraints();
		gbc_kdSpinnerB.fill = GridBagConstraints.HORIZONTAL;
		gbc_kdSpinnerB.insets = new Insets(0, 0, 5, 5);
		gbc_kdSpinnerB.gridx = 3;
		gbc_kdSpinnerB.gridy = 2;
		PIDPanel.add(kdSpinnerB, gbc_kdSpinnerB);

		kiSpinnerB = new JSpinner();
		kiSpinnerB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
		kiSpinnerB.setModel(new SpinnerNumberModel(new Double(0.0015), null, null, new Double(0.0001)));
		kiSpinnerB.setEditor(new JSpinner.NumberEditor(kiSpinnerB, "0.0000"));
		GridBagConstraints gbc_kiSpinnerB = new GridBagConstraints();
		gbc_kiSpinnerB.fill = GridBagConstraints.HORIZONTAL;
		gbc_kiSpinnerB.insets = new Insets(0, 0, 5, 5);
		gbc_kiSpinnerB.gridx = 4;
		gbc_kiSpinnerB.gridy = 2;
		PIDPanel.add(kiSpinnerB, gbc_kiSpinnerB);

		btnSendB = new JButton("send");
		btnSendB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendParameters(getSPB(), getKPB(), getKIB(), getKDB());
				query();
			}
		});
		btnSendB.setEnabled(false);
		GridBagConstraints gbc_btnSendB = new GridBagConstraints();
		gbc_btnSendB.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSendB.insets = new Insets(0, 0, 5, 5);
		gbc_btnSendB.gridx = 5;
		gbc_btnSendB.gridy = 2;
		PIDPanel.add(btnSendB, gbc_btnSendB);

		btnReadB = new JButton("read");
		btnReadB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				readB = true;
				query();
			}
		});
		btnReadB.setEnabled(false);
		GridBagConstraints gbc_btnReadB = new GridBagConstraints();
		gbc_btnReadB.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnReadB.insets = new Insets(0, 0, 5, 5);
		gbc_btnReadB.gridx = 6;
		gbc_btnReadB.gridy = 2;
		PIDPanel.add(btnReadB, gbc_btnReadB);

		lblGenerator = new JLabel("Wave Form");
		GridBagConstraints gbc_lblGenerator = new GridBagConstraints();
		gbc_lblGenerator.insets = new Insets(0, 0, 5, 5);
		gbc_lblGenerator.gridx = 1;
		gbc_lblGenerator.gridy = 3;
		PIDPanel.add(lblGenerator, gbc_lblGenerator);

		lblPeriod = new JLabel("Period(ms)");
		GridBagConstraints gbc_lblPeriod = new GridBagConstraints();
		gbc_lblPeriod.insets = new Insets(0, 0, 5, 5);
		gbc_lblPeriod.gridx = 2;
		gbc_lblPeriod.gridy = 3;
		PIDPanel.add(lblPeriod, gbc_lblPeriod);

		lblSteps = new JLabel("Steps");
		GridBagConstraints gbc_lblSteps = new GridBagConstraints();
		gbc_lblSteps.insets = new Insets(0, 0, 5, 5);
		gbc_lblSteps.gridx = 3;
		gbc_lblSteps.gridy = 3;
		PIDPanel.add(lblSteps, gbc_lblSteps);

		waveFormCombo = new JComboBox();
		waveFormCombo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				setModified(true);
			}
		});
		waveFormCombo.setModel(new DefaultComboBoxModel(new String[] { "Sin", "Triangle", "Square", "Pulse" }));
		GridBagConstraints gbc_waveFormCombo = new GridBagConstraints();
		gbc_waveFormCombo.insets = new Insets(0, 0, 0, 5);
		gbc_waveFormCombo.fill = GridBagConstraints.HORIZONTAL;
		gbc_waveFormCombo.gridx = 1;
		gbc_waveFormCombo.gridy = 4;
		PIDPanel.add(waveFormCombo, gbc_waveFormCombo);

		wavePeriodSpinner = new JSpinner();
		wavePeriodSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
		wavePeriodSpinner.setModel(new SpinnerNumberModel(new Integer(1000), null, null, new Integer(100)));
		wavePeriodSpinner.setEditor(new JSpinner.NumberEditor(wavePeriodSpinner, "#"));
		GridBagConstraints gbc_wavePeriodSpinner = new GridBagConstraints();
		gbc_wavePeriodSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_wavePeriodSpinner.insets = new Insets(0, 0, 0, 5);
		gbc_wavePeriodSpinner.gridx = 2;
		gbc_wavePeriodSpinner.gridy = 4;
		PIDPanel.add(wavePeriodSpinner, gbc_wavePeriodSpinner);

		waveStepsSpinner = new JSpinner();
		waveStepsSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
		waveStepsSpinner.setModel(new SpinnerNumberModel(new Integer(50), new Integer(1), null, new Integer(1)));
		waveStepsSpinner.setEditor(new JSpinner.NumberEditor(waveStepsSpinner, "#"));
		GridBagConstraints gbc_waveStepsSpinner = new GridBagConstraints();
		gbc_waveStepsSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_waveStepsSpinner.insets = new Insets(0, 0, 0, 5);
		gbc_waveStepsSpinner.gridx = 3;
		gbc_waveStepsSpinner.gridy = 4;
		PIDPanel.add(waveStepsSpinner, gbc_waveStepsSpinner);

		btnGenerate = new JButton("Start");
		btnGenerate.setEnabled(false);
		btnGenerate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (task == null) {
					generatorStart();
				} else {
					generatorStop();
				}
			}
		});
		GridBagConstraints gbc_btnGenerate = new GridBagConstraints();
		gbc_btnGenerate.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnGenerate.anchor = GridBagConstraints.EAST;
		gbc_btnGenerate.insets = new Insets(0, 0, 0, 5);
		gbc_btnGenerate.gridx = 5;
		gbc_btnGenerate.gridy = 4;
		PIDPanel.add(btnGenerate, gbc_btnGenerate);

		settingsPannel = new JPanel();
		settingsPannel
				.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Controller Settings", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_settingsPannel = new GridBagConstraints();
		gbc_settingsPannel.weightx = 1.0;
		gbc_settingsPannel.anchor = GridBagConstraints.WEST;
		gbc_settingsPannel.fill = GridBagConstraints.VERTICAL;
		gbc_settingsPannel.weighty = 1.0;
		gbc_settingsPannel.insets = new Insets(0, 0, 0, 5);
		gbc_settingsPannel.gridx = 1;
		gbc_settingsPannel.gridy = 0;
		bottomPanel.add(settingsPannel, gbc_settingsPannel);
		GridBagLayout gbl_settingsPannel = new GridBagLayout();
		gbl_settingsPannel.columnWidths = new int[] {50, 100, 75, 100, 75, 100};
		gbl_settingsPannel.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		gbl_settingsPannel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0 };
		settingsPannel.setLayout(gbl_settingsPannel);

		coilMinSpinner = new JSpinner();
		coilMinSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
						
						lblSensorReversed_1 = new JLabel("Sensor Reversed");
						GridBagConstraints gbc_lblSensorReversed_1 = new GridBagConstraints();
						gbc_lblSensorReversed_1.anchor = GridBagConstraints.SOUTH;
						gbc_lblSensorReversed_1.insets = new Insets(0, 0, 5, 5);
						gbc_lblSensorReversed_1.gridx = 0;
						gbc_lblSensorReversed_1.gridy = 0;
						settingsPannel.add(lblSensorReversed_1, gbc_lblSensorReversed_1);
						
						sensorReversedCheckBox = new JCheckBox("");
						sensorReversedCheckBox.addChangeListener(new ChangeListener() {
							public void stateChanged(ChangeEvent e) {
								setModified(true);
							}
						});
						GridBagConstraints gbc_sensorReversedCheckBox = new GridBagConstraints();
						gbc_sensorReversedCheckBox.anchor = GridBagConstraints.SOUTH;
						gbc_sensorReversedCheckBox.insets = new Insets(0, 0, 5, 5);
						gbc_sensorReversedCheckBox.gridx = 1;
						gbc_sensorReversedCheckBox.gridy = 0;
						settingsPannel.add(sensorReversedCheckBox, gbc_sensorReversedCheckBox);
				
						lblCtrlPeriod_1 = new JLabel("Ctrl Period");
						GridBagConstraints gbc_lblCtrlPeriod_1 = new GridBagConstraints();
						gbc_lblCtrlPeriod_1.weighty = 1.0;
						gbc_lblCtrlPeriod_1.anchor = GridBagConstraints.SOUTHEAST;
						gbc_lblCtrlPeriod_1.insets = new Insets(0, 0, 5, 5);
						gbc_lblCtrlPeriod_1.gridx = 2;
						gbc_lblCtrlPeriod_1.gridy = 0;
						settingsPannel.add(lblCtrlPeriod_1, gbc_lblCtrlPeriod_1);
				
						ctrlPeriodSpinner = new JSpinner();
						ctrlPeriodSpinner.addChangeListener(new ChangeListener() {
							public void stateChanged(ChangeEvent e) {
								setModified(true);
							}
						});
						GridBagConstraints gbc_ctrlPeriodSpinner = new GridBagConstraints();
						gbc_ctrlPeriodSpinner.fill = GridBagConstraints.HORIZONTAL;
						gbc_ctrlPeriodSpinner.insets = new Insets(0, 0, 5, 5);
						gbc_ctrlPeriodSpinner.anchor = GridBagConstraints.SOUTH;
						gbc_ctrlPeriodSpinner.gridx = 3;
						gbc_ctrlPeriodSpinner.gridy = 0;
						settingsPannel.add(ctrlPeriodSpinner, gbc_ctrlPeriodSpinner);
						ctrlPeriodSpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
						ctrlPeriodSpinner.setEditor(new JSpinner.NumberEditor(ctrlPeriodSpinner, "#"));
		
				JLabel lblCoilMin_2 = new JLabel("Coil Min");
				GridBagConstraints gbc_lblCoilMin_2 = new GridBagConstraints();
				gbc_lblCoilMin_2.anchor = GridBagConstraints.SOUTHEAST;
				gbc_lblCoilMin_2.insets = new Insets(0, 0, 5, 5);
				gbc_lblCoilMin_2.gridx = 4;
				gbc_lblCoilMin_2.gridy = 0;
				settingsPannel.add(lblCoilMin_2, gbc_lblCoilMin_2);
		coilMinSpinner.setModel(new SpinnerNumberModel(0, 0, 100, 1));
		GridBagConstraints gbc_coilMinSpinner = new GridBagConstraints();
		gbc_coilMinSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_coilMinSpinner.anchor = GridBagConstraints.SOUTH;
		gbc_coilMinSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_coilMinSpinner.gridx = 5;
		gbc_coilMinSpinner.gridy = 0;
		settingsPannel.add(coilMinSpinner, gbc_coilMinSpinner);

		horizontalGlue_1 = Box.createHorizontalGlue();
		GridBagConstraints gbc_horizontalGlue_1 = new GridBagConstraints();
		gbc_horizontalGlue_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalGlue_1.weightx = 10.0;
		gbc_horizontalGlue_1.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalGlue_1.gridx = 8;
		gbc_horizontalGlue_1.gridy = 0;
		settingsPannel.add(horizontalGlue_1, gbc_horizontalGlue_1);

		coilMidSpinner = new JSpinner();
		coilMidSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
						
								sensFilterSpinner = new JSpinner();
								sensFilterSpinner.addChangeListener(new ChangeListener() {
									public void stateChanged(ChangeEvent e) {
										setModified(true);
									}
								});
								
										lblSensFilter_1 = new JLabel("Sensor Filter");
										GridBagConstraints gbc_lblSensFilter_1 = new GridBagConstraints();
										gbc_lblSensFilter_1.anchor = GridBagConstraints.SOUTHEAST;
										gbc_lblSensFilter_1.insets = new Insets(0, 0, 5, 5);
										gbc_lblSensFilter_1.gridx = 0;
										gbc_lblSensFilter_1.gridy = 1;
										settingsPannel.add(lblSensFilter_1, gbc_lblSensFilter_1);
								GridBagConstraints gbc_sensFilterSpinner = new GridBagConstraints();
								gbc_sensFilterSpinner.anchor = GridBagConstraints.SOUTH;
								gbc_sensFilterSpinner.fill = GridBagConstraints.HORIZONTAL;
								gbc_sensFilterSpinner.insets = new Insets(0, 0, 5, 5);
								gbc_sensFilterSpinner.gridx = 1;
								gbc_sensFilterSpinner.gridy = 1;
								settingsPannel.add(sensFilterSpinner, gbc_sensFilterSpinner);
								sensFilterSpinner.setModel(new SpinnerNumberModel(new Integer(3), new Integer(1), null, new Integer(1)));
								sensFilterSpinner.setEditor(new JSpinner.NumberEditor(sensFilterSpinner, "#"));
				
						lblThreshold_1 = new JLabel("Actv Threshold");
						GridBagConstraints gbc_lblThreshold_1 = new GridBagConstraints();
						gbc_lblThreshold_1.anchor = GridBagConstraints.EAST;
						gbc_lblThreshold_1.insets = new Insets(0, 0, 5, 5);
						gbc_lblThreshold_1.gridx = 2;
						gbc_lblThreshold_1.gridy = 1;
						settingsPannel.add(lblThreshold_1, gbc_lblThreshold_1);
				
						actvThresholdSpinner = new JSpinner();
						actvThresholdSpinner.addChangeListener(new ChangeListener() {
							public void stateChanged(ChangeEvent e) {
								setModified(true);
							}
						});
						GridBagConstraints gbc_actvThresholdSpinner = new GridBagConstraints();
						gbc_actvThresholdSpinner.fill = GridBagConstraints.HORIZONTAL;
						gbc_actvThresholdSpinner.insets = new Insets(0, 0, 5, 5);
						gbc_actvThresholdSpinner.gridx = 3;
						gbc_actvThresholdSpinner.gridy = 1;
						settingsPannel.add(actvThresholdSpinner, gbc_actvThresholdSpinner);
						actvThresholdSpinner.setModel(new SpinnerNumberModel(325, 0, 1000, 1));
						actvThresholdSpinner.setEditor(new JSpinner.NumberEditor(actvThresholdSpinner, "#"));
		
				JLabel lblCoilMid_1 = new JLabel("Coil Mid");
				GridBagConstraints gbc_lblCoilMid_1 = new GridBagConstraints();
				gbc_lblCoilMid_1.anchor = GridBagConstraints.EAST;
				gbc_lblCoilMid_1.insets = new Insets(0, 0, 5, 5);
				gbc_lblCoilMid_1.gridx = 4;
				gbc_lblCoilMid_1.gridy = 1;
				settingsPannel.add(lblCoilMid_1, gbc_lblCoilMid_1);
		coilMidSpinner.setModel(new SpinnerNumberModel(50, 0, 100, 1));
		GridBagConstraints gbc_coilMidSpinner = new GridBagConstraints();
		gbc_coilMidSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_coilMidSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_coilMidSpinner.gridx = 5;
		gbc_coilMidSpinner.gridy = 1;
		settingsPannel.add(coilMidSpinner, gbc_coilMidSpinner);

		btnSettingsSend = new JButton("send");
		btnSettingsSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendSensReversed(getSensReversed());
				sendSensFilter(getSensFilter());
				sendCoilFilter(getCoilFilter());
				sendCtrlPeriod(getCtrlPeriod());
				sendThreshold(getActvThreshold());
				sendCtrlTimeout(getActvTimeout());
				sendCoilMin(getCoilMin());
				sendCoilMid(getCoilMid());
				sendCoilMax(getCoilMax());
				query();
			}
		});
				
						lblCoilFIlter_1 = new JLabel("Coil Filter");
						GridBagConstraints gbc_lblCoilFIlter_1 = new GridBagConstraints();
						gbc_lblCoilFIlter_1.anchor = GridBagConstraints.EAST;
						gbc_lblCoilFIlter_1.insets = new Insets(0, 0, 5, 5);
						gbc_lblCoilFIlter_1.gridx = 0;
						gbc_lblCoilFIlter_1.gridy = 2;
						settingsPannel.add(lblCoilFIlter_1, gbc_lblCoilFIlter_1);
				
						coilFilterSpinner = new JSpinner();
						coilFilterSpinner.addChangeListener(new ChangeListener() {
							public void stateChanged(ChangeEvent e) {
								setModified(true);
							}
						});
						GridBagConstraints gbc_coilFilterSpinner = new GridBagConstraints();
						gbc_coilFilterSpinner.fill = GridBagConstraints.HORIZONTAL;
						gbc_coilFilterSpinner.insets = new Insets(0, 0, 5, 5);
						gbc_coilFilterSpinner.gridx = 1;
						gbc_coilFilterSpinner.gridy = 2;
						settingsPannel.add(coilFilterSpinner, gbc_coilFilterSpinner);
						coilFilterSpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		
				lblTimeout = new JLabel("Actv Timeout");
				GridBagConstraints gbc_lblTimeout = new GridBagConstraints();
				gbc_lblTimeout.anchor = GridBagConstraints.EAST;
				gbc_lblTimeout.insets = new Insets(0, 0, 5, 5);
				gbc_lblTimeout.gridx = 2;
				gbc_lblTimeout.gridy = 2;
				settingsPannel.add(lblTimeout, gbc_lblTimeout);
		
				actvTimeoutSpinner = new JSpinner();
				actvTimeoutSpinner.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {
						setModified(true);
					}
				});
				GridBagConstraints gbc_actvTimeoutSpinner = new GridBagConstraints();
				gbc_actvTimeoutSpinner.fill = GridBagConstraints.HORIZONTAL;
				gbc_actvTimeoutSpinner.insets = new Insets(0, 0, 5, 5);
				gbc_actvTimeoutSpinner.gridx = 3;
				gbc_actvTimeoutSpinner.gridy = 2;
				settingsPannel.add(actvTimeoutSpinner, gbc_actvTimeoutSpinner);
				actvTimeoutSpinner.setModel(new SpinnerNumberModel(new Integer(2000), new Integer(1), null, new Integer(100)));
				actvTimeoutSpinner.setEditor(new JSpinner.NumberEditor(actvTimeoutSpinner, "#"));

		JLabel lblCoilMax_2 = new JLabel("Coil Max");
		GridBagConstraints gbc_lblCoilMax_2 = new GridBagConstraints();
		gbc_lblCoilMax_2.anchor = GridBagConstraints.EAST;
		gbc_lblCoilMax_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblCoilMax_2.gridx = 4;
		gbc_lblCoilMax_2.gridy = 2;
		settingsPannel.add(lblCoilMax_2, gbc_lblCoilMax_2);

		coilMaxSpinner = new JSpinner();
		coilMaxSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setModified(true);
			}
		});
		coilMaxSpinner.setModel(new SpinnerNumberModel(100, 0, 100, 1));
		GridBagConstraints gbc_coilMaxSpinner = new GridBagConstraints();
		gbc_coilMaxSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_coilMaxSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_coilMaxSpinner.gridx = 5;
		gbc_coilMaxSpinner.gridy = 2;
		settingsPannel.add(coilMaxSpinner, gbc_coilMaxSpinner);
		btnSettingsSend.setEnabled(false);
		GridBagConstraints gbc_btnSettingsSend = new GridBagConstraints();
		gbc_btnSettingsSend.weighty = 1.0;
		gbc_btnSettingsSend.anchor = GridBagConstraints.SOUTHEAST;
		gbc_btnSettingsSend.insets = new Insets(0, 0, 0, 5);
		gbc_btnSettingsSend.gridx = 0;
		gbc_btnSettingsSend.gridy = 3;
		settingsPannel.add(btnSettingsSend, gbc_btnSettingsSend);

		btnSettingsRead = new JButton("read");
		btnSettingsRead.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				readSettings = true;
				query();
			}
		});
		btnSettingsRead.setEnabled(false);
		GridBagConstraints gbc_btnSettingsRead = new GridBagConstraints();
		gbc_btnSettingsRead.anchor = GridBagConstraints.SOUTHWEST;
		gbc_btnSettingsRead.insets = new Insets(0, 0, 0, 5);
		gbc_btnSettingsRead.gridx = 1;
		gbc_btnSettingsRead.gridy = 3;
		settingsPannel.add(btnSettingsRead, gbc_btnSettingsRead);

		axisPanel = new JPanel();
		rightPane.add(axisPanel, BorderLayout.NORTH);
		GridBagLayout gbl_axisPanel = new GridBagLayout();
		axisPanel.setLayout(gbl_axisPanel);

		JPanel sensorAxisPanel = new JPanel();
		sensorAxisPanel
				.setBorder(new TitledBorder(null, "SensorAxis", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		sensorAxisPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		GridBagConstraints gbc_sensorAxisPanel = new GridBagConstraints();
		gbc_sensorAxisPanel.weightx = 1.0;
		gbc_sensorAxisPanel.anchor = GridBagConstraints.NORTHEAST;
		gbc_sensorAxisPanel.insets = new Insets(0, 0, 0, 5);
		gbc_sensorAxisPanel.gridx = 1;
		gbc_sensorAxisPanel.gridy = 0;
		axisPanel.add(sensorAxisPanel, gbc_sensorAxisPanel);
		GridBagLayout gbl_sensorAxisPanel = new GridBagLayout();
		gbl_sensorAxisPanel.columnWidths = new int[] { 30, 54, 30, 54 };
		gbl_sensorAxisPanel.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0 };
		gbl_sensorAxisPanel.rowWeights = new double[] { 0.0 };
		sensorAxisPanel.setLayout(gbl_sensorAxisPanel);

		JLabel lblSensorMin = new JLabel("min");
		GridBagConstraints gbc_lblSensorMin = new GridBagConstraints();
		gbc_lblSensorMin.anchor = GridBagConstraints.EAST;
		gbc_lblSensorMin.fill = GridBagConstraints.VERTICAL;
		gbc_lblSensorMin.insets = new Insets(5, 5, 5, 5);
		gbc_lblSensorMin.gridx = 0;
		gbc_lblSensorMin.gridy = 0;
		sensorAxisPanel.add(lblSensorMin, gbc_lblSensorMin);

		sensorAxisMinSpinner = new JSpinner();
		sensorAxisMinSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int min = ((Number) sensorAxisMinSpinner.getValue()).intValue();
				int max = ((Number) sensorAxisMaxSpinner.getValue()).intValue();
				sensorAxis.setInverted((max < min));
				if(max<min) {
					int m= min;
					min=max;
					max=m;
				}
//					max = min + 1;
				sensorAxis.setRange(min, max);
			}
		});
		sensorAxisMinSpinner.setModel(new SpinnerNumberModel(0, 0, 1023, 1));
		sensorAxisMinSpinner.setEditor(new JSpinner.NumberEditor(sensorAxisMinSpinner, "#"));
		GridBagConstraints gbc_sensorAxisMinSpinner = new GridBagConstraints();
		gbc_sensorAxisMinSpinner.fill = GridBagConstraints.BOTH;
		gbc_sensorAxisMinSpinner.gridx = 1;
		gbc_sensorAxisMinSpinner.gridy = 0;
		sensorAxisPanel.add(sensorAxisMinSpinner, gbc_sensorAxisMinSpinner);

		JLabel lblSensorMax = new JLabel("max");
		GridBagConstraints gbc_lblSensorMax = new GridBagConstraints();
		gbc_lblSensorMax.anchor = GridBagConstraints.EAST;
		gbc_lblSensorMax.fill = GridBagConstraints.VERTICAL;
		gbc_lblSensorMax.insets = new Insets(5, 5, 5, 5);
		gbc_lblSensorMax.gridx = 2;
		gbc_lblSensorMax.gridy = 0;
		sensorAxisPanel.add(lblSensorMax, gbc_lblSensorMax);

		sensorAxisMaxSpinner = new JSpinner();
		sensorAxisMaxSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int min = ((Number) sensorAxisMinSpinner.getValue()).intValue();
				int max = ((Number) sensorAxisMaxSpinner.getValue()).intValue();
				sensorAxis.setInverted((max < min));
				if(max<min) {
					int m= min;
					min=max;
					max=m;
				}
//				if (min >= max)
//					min = max - 1;
				sensorAxis.setRange(min, max);
			}
		});
		sensorAxisMaxSpinner.setModel(new SpinnerNumberModel(1024, 1, 1024, 1));
		sensorAxisMaxSpinner.setEditor(new JSpinner.NumberEditor(sensorAxisMaxSpinner, "#"));
		GridBagConstraints gbc_sensorAxisMaxSpinner = new GridBagConstraints();
		gbc_sensorAxisMaxSpinner.fill = GridBagConstraints.BOTH;
		gbc_sensorAxisMaxSpinner.gridx = 3;
		gbc_sensorAxisMaxSpinner.gridy = 0;
		sensorAxisPanel.add(sensorAxisMaxSpinner, gbc_sensorAxisMaxSpinner);

		coilAxisPanel = new JPanel();
		coilAxisPanel.setBorder(new TitledBorder(null, "CoilAxis", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		coilAxisPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		coilAxisPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		GridBagConstraints gbc_coilAxisPanel = new GridBagConstraints();
		gbc_coilAxisPanel.weightx = 1.0;
		gbc_coilAxisPanel.anchor = GridBagConstraints.NORTHWEST;
		gbc_coilAxisPanel.gridx = 0;
		gbc_coilAxisPanel.gridy = 0;
		axisPanel.add(coilAxisPanel, gbc_coilAxisPanel);
		GridBagLayout gbl_coilAxisPanel = new GridBagLayout();
		gbl_coilAxisPanel.columnWidths = new int[] { 30, 54, 30, 54 };
		coilAxisPanel.setLayout(gbl_coilAxisPanel);

		JLabel lblCoilMin = new JLabel("min");
		GridBagConstraints gbc_lblCoilMin = new GridBagConstraints();
		gbc_lblCoilMin.insets = new Insets(0, 0, 5, 5);
		gbc_lblCoilMin.gridx = 0;
		gbc_lblCoilMin.gridy = 0;
		coilAxisPanel.add(lblCoilMin, gbc_lblCoilMin);

		coilAxisMinSpinner = new JSpinner();
		GridBagConstraints gbc_coilAxisMinSpinner = new GridBagConstraints();
		gbc_coilAxisMinSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_coilAxisMinSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_coilAxisMinSpinner.gridx = 1;
		gbc_coilAxisMinSpinner.gridy = 0;
		coilAxisPanel.add(coilAxisMinSpinner, gbc_coilAxisMinSpinner);
		coilAxisMinSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int min = ((Number) coilAxisMinSpinner.getValue()).intValue();
				int max = ((Number) coilAxisMaxSpinner.getValue()).intValue();
				coilAxis.setInverted((max < min));
				if(max<min) {
					int m= min;
					min=max;
					max=m;
				}
				coilAxis.setRange(min, max);
			}
		});
		coilAxisMinSpinner.setModel(new SpinnerNumberModel(0, 0, 100, 1));
		coilAxisMinSpinner.setEditor(new JSpinner.NumberEditor(coilAxisMinSpinner, "#"));

		lblCoilMax = new JLabel("max");
		GridBagConstraints gbc_lblCoilMax = new GridBagConstraints();
		gbc_lblCoilMax.insets = new Insets(0, 0, 5, 5);
		gbc_lblCoilMax.gridx = 2;
		gbc_lblCoilMax.gridy = 0;
		coilAxisPanel.add(lblCoilMax, gbc_lblCoilMax);

		coilAxisMaxSpinner = new JSpinner();
		GridBagConstraints gbc_coilAxisMaxSpinner = new GridBagConstraints();
		gbc_coilAxisMaxSpinner.weightx = 10.0;
		gbc_coilAxisMaxSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_coilAxisMaxSpinner.insets = new Insets(0, 0, 5, 0);
		gbc_coilAxisMaxSpinner.gridx = 3;
		gbc_coilAxisMaxSpinner.gridy = 0;
		coilAxisPanel.add(coilAxisMaxSpinner, gbc_coilAxisMaxSpinner);
		coilAxisMaxSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int min = ((Number) coilAxisMinSpinner.getValue()).intValue();
				int max = ((Number) coilAxisMaxSpinner.getValue()).intValue();
				coilAxis.setInverted((max < min));
				if(max<min) {
					int m= min;
					min=max;
					max=m;
				}
				coilAxis.setRange(min, max);
			}
		});
		coilAxisMaxSpinner.setModel(new SpinnerNumberModel(100, 0, 100, 1));
		coilAxisMaxSpinner.setEditor(new JSpinner.NumberEditor(coilAxisMaxSpinner, "#"));

		leftPanel = new JPanel();
		leftPanel.setMinimumSize(new Dimension(250, 100));
		leftPanel.setPreferredSize(new Dimension(250, 100));
		frame.getContentPane().add(leftPanel, BorderLayout.WEST);
		leftPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		SpringLayout sl_leftPanel = new SpringLayout();
		leftPanel.setLayout(sl_leftPanel);

		serialPanel = new JPanel();
		sl_leftPanel.putConstraint(SpringLayout.NORTH, serialPanel, 0, SpringLayout.NORTH, leftPanel);
		sl_leftPanel.putConstraint(SpringLayout.WEST, serialPanel, 0, SpringLayout.WEST, leftPanel);
		sl_leftPanel.putConstraint(SpringLayout.EAST, serialPanel, 0, SpringLayout.EAST, leftPanel);
		leftPanel.add(serialPanel);
		serialPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		serialPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Controller",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagLayout gbl_serialPanel = new GridBagLayout();
		gbl_serialPanel.columnWidths = new int[] { 150, 70 };
		serialPanel.setLayout(gbl_serialPanel);

		serialPortCombo = new JComboBox();
		GridBagConstraints gbc_serialPortCombo = new GridBagConstraints();
		gbc_serialPortCombo.weightx = 10.0;
		gbc_serialPortCombo.fill = GridBagConstraints.HORIZONTAL;
		gbc_serialPortCombo.insets = new Insets(0, 0, 5, 5);
		gbc_serialPortCombo.gridx = 0;
		gbc_serialPortCombo.gridy = 0;
		serialPanel.add(serialPortCombo, gbc_serialPortCombo);

		btnRefresh = new JButton("refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshPorts();
			}
		});
		GridBagConstraints gbc_btnRefresh = new GridBagConstraints();
		gbc_btnRefresh.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRefresh.insets = new Insets(0, 0, 5, 0);
		gbc_btnRefresh.gridx = 1;
		gbc_btnRefresh.gridy = 0;
		serialPanel.add(btnRefresh, gbc_btnRefresh);

		tglbtnConnect = new JToggleButton("Connect");
		GridBagConstraints gbc_tglbtnConnect = new GridBagConstraints();
		gbc_tglbtnConnect.insets = new Insets(0, 0, 5, 0);
		gbc_tglbtnConnect.fill = GridBagConstraints.HORIZONTAL;
		gbc_tglbtnConnect.gridwidth = 2;
		gbc_tglbtnConnect.gridx = 0;
		gbc_tglbtnConnect.gridy = 1;
		serialPanel.add(tglbtnConnect, gbc_tglbtnConnect);

		controllerPanel = new JPanel();
		sl_leftPanel.putConstraint(SpringLayout.NORTH, controllerPanel, 0, SpringLayout.SOUTH, serialPanel);
		sl_leftPanel.putConstraint(SpringLayout.WEST, controllerPanel, 0, SpringLayout.WEST, leftPanel);
		sl_leftPanel.putConstraint(SpringLayout.EAST, controllerPanel, 0, SpringLayout.EAST, leftPanel);
		leftPanel.add(controllerPanel);
		controllerPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Levitator", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagLayout gbl_controllerPanel = new GridBagLayout();
		gbl_controllerPanel.columnWidths = new int[] { 120, 150 };
		gbl_controllerPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0 };
		gbl_controllerPanel.columnWeights = new double[] { 1.0, 1.0 };
		controllerPanel.setLayout(gbl_controllerPanel);

		lblCtrlPeriod = new JLabel("Period");
		GridBagConstraints gbc_lblCtrlPeriod = new GridBagConstraints();
		gbc_lblCtrlPeriod.anchor = GridBagConstraints.EAST;
		gbc_lblCtrlPeriod.insets = new Insets(0, 0, 5, 5);
		gbc_lblCtrlPeriod.gridx = 0;
		gbc_lblCtrlPeriod.gridy = 0;
		controllerPanel.add(lblCtrlPeriod, gbc_lblCtrlPeriod);

		ctrlPeriodTextField = new JTextField();
		ctrlPeriodTextField.setHorizontalAlignment(SwingConstants.CENTER);
		ctrlPeriodTextField.setEditable(false);
		GridBagConstraints gbc_ctrlPeriodTextField = new GridBagConstraints();
		gbc_ctrlPeriodTextField.weightx = 1.0;
		gbc_ctrlPeriodTextField.insets = new Insets(0, 0, 5, 0);
		gbc_ctrlPeriodTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_ctrlPeriodTextField.gridx = 1;
		gbc_ctrlPeriodTextField.gridy = 0;
		controllerPanel.add(ctrlPeriodTextField, gbc_ctrlPeriodTextField);
		ctrlPeriodTextField.setColumns(10);

		lblThreshold = new JLabel("Threshold");
		GridBagConstraints gbc_lblThreshold = new GridBagConstraints();
		gbc_lblThreshold.anchor = GridBagConstraints.EAST;
		gbc_lblThreshold.insets = new Insets(0, 0, 5, 5);
		gbc_lblThreshold.gridx = 0;
		gbc_lblThreshold.gridy = 1;
		controllerPanel.add(lblThreshold, gbc_lblThreshold);

		thresholdTextField = new JTextField();
		thresholdTextField.setHorizontalAlignment(SwingConstants.CENTER);
		thresholdTextField.setEditable(false);
		GridBagConstraints gbc_thresholdTextField = new GridBagConstraints();
		gbc_thresholdTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_thresholdTextField.anchor = GridBagConstraints.NORTH;
		gbc_thresholdTextField.insets = new Insets(0, 0, 5, 0);
		gbc_thresholdTextField.gridx = 1;
		gbc_thresholdTextField.gridy = 1;
		controllerPanel.add(thresholdTextField, gbc_thresholdTextField);
		thresholdTextField.setColumns(10);

		lblCtrlTimeout = new JLabel("Timeout");
		GridBagConstraints gbc_lblCtrlTimeout = new GridBagConstraints();
		gbc_lblCtrlTimeout.anchor = GridBagConstraints.EAST;
		gbc_lblCtrlTimeout.insets = new Insets(0, 0, 5, 5);
		gbc_lblCtrlTimeout.gridx = 0;
		gbc_lblCtrlTimeout.gridy = 2;
		controllerPanel.add(lblCtrlTimeout, gbc_lblCtrlTimeout);

		ctrlTimeoutTextField = new JTextField();
		ctrlTimeoutTextField.setHorizontalAlignment(SwingConstants.CENTER);
		ctrlTimeoutTextField.setEditable(false);
		GridBagConstraints gbc_ctrlTimeoutTextField = new GridBagConstraints();
		gbc_ctrlTimeoutTextField.insets = new Insets(0, 0, 5, 0);
		gbc_ctrlTimeoutTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_ctrlTimeoutTextField.gridx = 1;
		gbc_ctrlTimeoutTextField.gridy = 2;
		controllerPanel.add(ctrlTimeoutTextField, gbc_ctrlTimeoutTextField);
		ctrlTimeoutTextField.setColumns(10);
		
		lblSensorReversed = new JLabel("Sensor reversed");
		GridBagConstraints gbc_lblSensorReversed = new GridBagConstraints();
		gbc_lblSensorReversed.weighty = 1.0;
		gbc_lblSensorReversed.anchor = GridBagConstraints.EAST;
		gbc_lblSensorReversed.insets = new Insets(0, 0, 5, 5);
		gbc_lblSensorReversed.gridx = 0;
		gbc_lblSensorReversed.gridy = 3;
		controllerPanel.add(lblSensorReversed, gbc_lblSensorReversed);
		
		sensorReversedChecked = new JCheckBox("");
		sensorReversedChecked.setEnabled(false);
		GridBagConstraints gbc_sensorReversedChecked = new GridBagConstraints();
		gbc_sensorReversedChecked.insets = new Insets(0, 0, 5, 0);
		gbc_sensorReversedChecked.gridx = 1;
		gbc_sensorReversedChecked.gridy = 3;
		controllerPanel.add(sensorReversedChecked, gbc_sensorReversedChecked);

		lblSensFilter = new JLabel("Sensor Filter");
		GridBagConstraints gbc_lblSensFilter = new GridBagConstraints();
		gbc_lblSensFilter.anchor = GridBagConstraints.EAST;
		gbc_lblSensFilter.insets = new Insets(0, 0, 5, 5);
		gbc_lblSensFilter.gridx = 0;
		gbc_lblSensFilter.gridy = 4;
		controllerPanel.add(lblSensFilter, gbc_lblSensFilter);

		sensFilterTextField = new JTextField();
		sensFilterTextField.setHorizontalAlignment(SwingConstants.CENTER);
		sensFilterTextField.setEditable(false);
		GridBagConstraints gbc_sensFilterTextField = new GridBagConstraints();
		gbc_sensFilterTextField.insets = new Insets(0, 0, 5, 0);
		gbc_sensFilterTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_sensFilterTextField.gridx = 1;
		gbc_sensFilterTextField.gridy = 4;
		controllerPanel.add(sensFilterTextField, gbc_sensFilterTextField);
		sensFilterTextField.setColumns(10);

		lblCoilFilter = new JLabel("Coil Filter");
		GridBagConstraints gbc_lblCoilFilter = new GridBagConstraints();
		gbc_lblCoilFilter.anchor = GridBagConstraints.EAST;
		gbc_lblCoilFilter.insets = new Insets(0, 0, 5, 5);
		gbc_lblCoilFilter.gridx = 0;
		gbc_lblCoilFilter.gridy = 5;
		controllerPanel.add(lblCoilFilter, gbc_lblCoilFilter);

		coilFilterTextField = new JTextField();
		coilFilterTextField.setHorizontalAlignment(SwingConstants.CENTER);
		coilFilterTextField.setEditable(false);
		GridBagConstraints gbc_coilFilterTextField = new GridBagConstraints();
		gbc_coilFilterTextField.insets = new Insets(0, 0, 5, 0);
		gbc_coilFilterTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_coilFilterTextField.gridx = 1;
		gbc_coilFilterTextField.gridy = 5;
		controllerPanel.add(coilFilterTextField, gbc_coilFilterTextField);
		coilFilterTextField.setColumns(10);

		lblCoilMin_1 = new JLabel("Coil Min");
		GridBagConstraints gbc_lblCoilMin_1 = new GridBagConstraints();
		gbc_lblCoilMin_1.anchor = GridBagConstraints.EAST;
		gbc_lblCoilMin_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblCoilMin_1.gridx = 0;
		gbc_lblCoilMin_1.gridy = 6;
		controllerPanel.add(lblCoilMin_1, gbc_lblCoilMin_1);

		coilMinTextField = new JTextField();
		coilMinTextField.setHorizontalAlignment(SwingConstants.CENTER);
		coilMinTextField.setEditable(false);
		GridBagConstraints gbc_coilMinTextField = new GridBagConstraints();
		gbc_coilMinTextField.insets = new Insets(0, 0, 5, 0);
		gbc_coilMinTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_coilMinTextField.gridx = 1;
		gbc_coilMinTextField.gridy = 6;
		controllerPanel.add(coilMinTextField, gbc_coilMinTextField);
		coilMinTextField.setColumns(10);

		JLabel lblCoilMid = new JLabel("Coil Mid");
		GridBagConstraints gbc_lblCoilMid = new GridBagConstraints();
		gbc_lblCoilMid.anchor = GridBagConstraints.EAST;
		gbc_lblCoilMid.insets = new Insets(0, 0, 5, 5);
		gbc_lblCoilMid.gridx = 0;
		gbc_lblCoilMid.gridy = 7;
		controllerPanel.add(lblCoilMid, gbc_lblCoilMid);

		coilMidTextField = new JTextField();
		coilMidTextField.setHorizontalAlignment(SwingConstants.CENTER);
		coilMidTextField.setEditable(false);
		GridBagConstraints gbc_coilMidTextField = new GridBagConstraints();
		gbc_coilMidTextField.insets = new Insets(0, 0, 5, 0);
		gbc_coilMidTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_coilMidTextField.gridx = 1;
		gbc_coilMidTextField.gridy = 7;
		controllerPanel.add(coilMidTextField, gbc_coilMidTextField);
		coilMidTextField.setColumns(10);

		JLabel lblCoilMax_1 = new JLabel("Coil Max");
		GridBagConstraints gbc_lblCoilMax_1 = new GridBagConstraints();
		gbc_lblCoilMax_1.anchor = GridBagConstraints.EAST;
		gbc_lblCoilMax_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblCoilMax_1.gridx = 0;
		gbc_lblCoilMax_1.gridy = 8;
		controllerPanel.add(lblCoilMax_1, gbc_lblCoilMax_1);

		coilMaxTextField = new JTextField();
		coilMaxTextField.setHorizontalAlignment(SwingConstants.CENTER);
		coilMaxTextField.setEditable(false);
		GridBagConstraints gbc_coilMaxTextField = new GridBagConstraints();
		gbc_coilMaxTextField.insets = new Insets(0, 0, 5, 0);
		gbc_coilMaxTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_coilMaxTextField.gridx = 1;
		gbc_coilMaxTextField.gridy = 8;
		controllerPanel.add(coilMaxTextField, gbc_coilMaxTextField);
		coilMaxTextField.setColumns(10);

		lblSetPoint = new JLabel("Set Point");
		GridBagConstraints gbc_lblSetPoint = new GridBagConstraints();
		gbc_lblSetPoint.insets = new Insets(0, 0, 5, 5);
		gbc_lblSetPoint.anchor = GridBagConstraints.EAST;
		gbc_lblSetPoint.gridx = 0;
		gbc_lblSetPoint.gridy = 9;
		controllerPanel.add(lblSetPoint, gbc_lblSetPoint);

		spTextField = new JTextField();
		GridBagConstraints gbc_spTextField = new GridBagConstraints();
		gbc_spTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_spTextField.insets = new Insets(0, 0, 5, 0);
		gbc_spTextField.gridx = 1;
		gbc_spTextField.gridy = 9;
		controllerPanel.add(spTextField, gbc_spTextField);
		spTextField.setHorizontalAlignment(SwingConstants.CENTER);
		spTextField.setEditable(false);
		spTextField.setColumns(10);

		lblKp = new JLabel("KP");
		GridBagConstraints gbc_lblKp = new GridBagConstraints();
		gbc_lblKp.insets = new Insets(0, 0, 5, 5);
		gbc_lblKp.anchor = GridBagConstraints.EAST;
		gbc_lblKp.gridx = 0;
		gbc_lblKp.gridy = 10;
		controllerPanel.add(lblKp, gbc_lblKp);

		kpTextField = new JTextField();
		GridBagConstraints gbc_kpTextField = new GridBagConstraints();
		gbc_kpTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_kpTextField.insets = new Insets(0, 0, 5, 0);
		gbc_kpTextField.gridx = 1;
		gbc_kpTextField.gridy = 10;
		controllerPanel.add(kpTextField, gbc_kpTextField);
		kpTextField.setHorizontalAlignment(SwingConstants.CENTER);
		kpTextField.setEditable(false);
		kpTextField.setColumns(10);

		lblKd = new JLabel("KD");
		GridBagConstraints gbc_lblKd = new GridBagConstraints();
		gbc_lblKd.insets = new Insets(0, 0, 5, 5);
		gbc_lblKd.anchor = GridBagConstraints.EAST;
		gbc_lblKd.gridx = 0;
		gbc_lblKd.gridy = 11;
		controllerPanel.add(lblKd, gbc_lblKd);

		kdTextField = new JTextField();
		GridBagConstraints gbc_kdTextField = new GridBagConstraints();
		gbc_kdTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_kdTextField.insets = new Insets(0, 0, 5, 0);
		gbc_kdTextField.gridx = 1;
		gbc_kdTextField.gridy = 11;
		controllerPanel.add(kdTextField, gbc_kdTextField);
		kdTextField.setHorizontalAlignment(SwingConstants.CENTER);
		kdTextField.setEditable(false);
		kdTextField.setColumns(10);

		lblKi = new JLabel("KI");
		GridBagConstraints gbc_lblKi = new GridBagConstraints();
		gbc_lblKi.insets = new Insets(0, 0, 5, 5);
		gbc_lblKi.anchor = GridBagConstraints.EAST;
		gbc_lblKi.gridx = 0;
		gbc_lblKi.gridy = 12;
		controllerPanel.add(lblKi, gbc_lblKi);

		kiTextField = new JTextField();
		GridBagConstraints gbc_kiTextField = new GridBagConstraints();
		gbc_kiTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_kiTextField.insets = new Insets(0, 0, 5, 0);
		gbc_kiTextField.gridx = 1;
		gbc_kiTextField.gridy = 12;
		controllerPanel.add(kiTextField, gbc_kiTextField);
		kiTextField.setHorizontalAlignment(SwingConstants.CENTER);
		kiTextField.setEditable(false);
		kiTextField.setColumns(10);

		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.anchor = GridBagConstraints.NORTH;
		gbc_panel.gridwidth = 2;
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 13;
		controllerPanel.add(panel, gbc_panel);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 150, 150 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0 };
		gridBagLayout.rowWeights = new double[] { 0.0 };
		panel.setLayout(gridBagLayout);

		btnSaveDefault = new JButton(">EEPROM");
		btnSaveDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendSave();
			}
		});
		btnSaveDefault.setEnabled(false);
		GridBagConstraints gbc_btnSaveDefault = new GridBagConstraints();
		gbc_btnSaveDefault.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSaveDefault.weightx = 1.0;
		gbc_btnSaveDefault.anchor = GridBagConstraints.NORTH;
		gbc_btnSaveDefault.insets = new Insets(0, 0, 0, 5);
		gbc_btnSaveDefault.gridx = 0;
		gbc_btnSaveDefault.gridy = 0;
		panel.add(btnSaveDefault, gbc_btnSaveDefault);

		btnRestoreDefault = new JButton("EEPROM>");
		btnRestoreDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendRestore();
			}
		});
		btnRestoreDefault.setEnabled(false);
		GridBagConstraints gbc_btnRestoreDefault = new GridBagConstraints();
		gbc_btnRestoreDefault.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRestoreDefault.weightx = 1.0;
		gbc_btnRestoreDefault.gridx = 1;
		gbc_btnRestoreDefault.gridy = 0;
		panel.add(btnRestoreDefault, gbc_btnRestoreDefault);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic('F');
		menuBar.add(mnFile);

		JMenuItem mntmOpen = new JMenuItem("Open");
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doFileOpen();
			}
		});
		mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mntmOpen.setMnemonic('O');
		mnFile.add(mntmOpen);

		mnFile.add(new JSeparator());

		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doFileSave();
			}
		});
		mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mntmSave.setMnemonic('S');
		mnFile.add(mntmSave);

		JMenuItem mntmSaveAs = new JMenuItem("Save as");
		mntmSaveAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doFileSaveAs();
			}
		});
		mnFile.add(mntmSaveAs);
		
		mnFile.add(new JSeparator());

		mntmSaveAsDefaults = new JMenuItem("Save As Defaults");
		mntmSaveAsDefaults.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveAsDefaults();
			}
		});
		mnFile.add(mntmSaveAsDefaults);

		mntmRestoreDefaults = new JMenuItem("Restore Defaults");
		mntmRestoreDefaults.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadDefaults();
			}
		});
		mnFile.add(mntmRestoreDefaults);
		
		separator = new JSeparator();
		mnFile.add(separator);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doExit();
			}
		});
		
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
		mntmExit.setMnemonic('x');
		mnFile.add(mntmExit);
		
		JMenu mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);
		
		chckbxmntmLoadPidA = new JCheckBoxMenuItem("Load PID A on connection");
		mnOptions.add(chckbxmntmLoadPidA);
		
		chckbxmntmLoadPidB = new JCheckBoxMenuItem("Load PID B on connection");
		mnOptions.add(chckbxmntmLoadPidB);
		
		chckbxmntmLoadSettingsUpon = new JCheckBoxMenuItem("Load Controller Settings on connection");
		mnOptions.add(chckbxmntmLoadSettingsUpon);

		JMenu menu = new JMenu("?");
		menuBar.add(menu);

		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frame, "Levitator V"+VERSION+"\nBy RxBConcept", "About", JOptionPane.CANCEL_OPTION);
			}
		});
		mntmAbout.setMnemonic('A');
		menu.add(mntmAbout);

		tglbtnConnect.addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent e) {
				if (!tglbtnConnect.isSelected()) {
					disconnect();
				} else {
					connect_();
				}
			}
		});

	}

	private void disconnect() {
		tglbtnConnect.setText("Connect");
		if (serialPort != null) {
			connected = false;
			try {
				report(0);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				serialPort.closePort();
			} catch (SerialPortException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
			}
			serialPort = null;
			System.out.println("Disconnected...");

			
			
			btnSaveDefault.setEnabled(false);
			btnRestoreDefault.setEnabled(false);

			btnSettingsSend.setEnabled(false);
			btnSettingsRead.setEnabled(false);

			// btnCtrlPeriodSend.setEnabled(false);
			// btnMagThresholdSend.setEnabled(false);
			// btnMidPointSend.setEnabled(false);
			// btnFilterFactorSend.setEnabled(false);
			// btnCtrlTimeoutSend.setEnabled(false);
			// btnQuietModeSend.setEnabled(false);

			btnSendA.setEnabled(false);
			btnSendB.setEnabled(false);
			btnGenerate.setEnabled(false);
			btnReadA.setEnabled(false);
			btnReadB.setEnabled(false);

			btnRefresh.setEnabled(true);
			serialPortCombo.setEnabled(true);
			// refreshPorts();
		}		
	}
	
	private void connect_() {
		if (serialPort == null) {
			String port = (String) serialPortCombo.getSelectedItem();
			if (port == null) {
				tglbtnConnect.setSelected(false);
				return;
			}
			btnRefresh.setEnabled(false);
			serialPortCombo.setEnabled(false);
			tglbtnConnect.setText("Disconnect");

			serialPort = new SerialPort(port);
			new SwingWorker<Boolean, Void>() {
				@Override
				public Boolean doInBackground() {
					try {
						serialPort.openPort();
						serialPort.setParams(115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
								SerialPort.PARITY_NONE, false, false);
						serialPort.addEventListener(Main.this);
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("Connected...");
						watchdog= new TimerTask() {
							public void run() {
								//System.out.println("Alive");
								if(System.currentTimeMillis()-lastReportDate>1000) {
									doDisconnect();
									cancel();
									timer.purge();
								}
							}
						};
						lastReportDate= System.currentTimeMillis();
						timer.schedule(watchdog, 1000, 1000);
						readSettings=chckbxmntmLoadSettingsUpon.isSelected();
						readA= chckbxmntmLoadPidA.isSelected();
						readB= chckbxmntmLoadPidB.isSelected();
						query();
						return Boolean.TRUE;
					} catch (SerialPortException ex) {
						// TODO Auto-generated catch block
						return Boolean.FALSE;
					}
				}

				@Override
				public void done() {
					try {
						if (get()) {
							connected = true;
							btnSaveDefault.setEnabled(true);
							btnRestoreDefault.setEnabled(true);

							btnSettingsSend.setEnabled(true);
							btnSettingsRead.setEnabled(true);

							btnSendA.setEnabled(true);
							btnSendB.setEnabled(true);
							btnGenerate.setEnabled(true);
							btnReadA.setEnabled(true);
							btnReadB.setEnabled(true);
							report(REPORT_PERIOD);

						} else {
							tglbtnConnect.setSelected(false);
						}
					} catch (InterruptedException e) {
						tglbtnConnect.setSelected(false);
					} catch (ExecutionException e) {
						tglbtnConnect.setSelected(false);
					}
				}
			}.execute();
		}
	}
	
	
	private String getTitle() {
		return "Levitator "+VERSION + (configFile == null ? "" : " - " + configFile.getPath());
	}

	private void setModified(boolean modified) {
		this.modified = modified;
		frame.setTitle(getTitle() + (modified ? " *" : ""));
	}

	private void doFileOpen() {
		if(modified) {
			int val = JOptionPane.showConfirmDialog(frame, "Discard changes?", "Configuration has been modified", JOptionPane.YES_NO_OPTION,
					JOptionPane.WARNING_MESSAGE);
			if (val != JOptionPane.YES_OPTION) {
				return;
			}
		}
		
		fc.setFileFilter(new FileNameExtensionFilter("Levitator file", "lvt", "LVT"));
		int returnVal = fc.showOpenDialog(frame);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();

			if (!doOpen(file)) {
				JOptionPane.showMessageDialog(frame, "Failed to open file");
			} else {
				this.configFile = file;
				setModified(false);
			}
		}
	}

	private void doFileSave() {
		if (configFile != null) {
			doSave(configFile);
		} else {
			doFileSaveAs();
		}
	}

	private void doFileSaveAs() {
		fc.setFileFilter(new FileNameExtensionFilter("Levitator file", "lvt", "LVT"));
		int returnVal = fc.showSaveDialog(frame);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			if(!file.getName().toUpperCase().endsWith(".LVT")) file= new File(file.getParent(), file.getName()+".lvt");
			if (file.exists()) {
				int val = JOptionPane.showConfirmDialog(frame, "Overrite it?", "File exist", JOptionPane.YES_NO_OPTION,
						JOptionPane.WARNING_MESSAGE);
				if (val != JOptionPane.YES_OPTION) {
					return;
				}
			}
			if (!doSave(file)) {
				JOptionPane.showMessageDialog(frame, "Unable to save file");
			}
		}

	}

	private boolean doSave(File file) {
		if (file == null)	return false;

		Properties config = new Properties();
		config.setProperty("VERSION", "1.1");
		config.setProperty("SPA", String.format("%d", getSPA()));
		config.setProperty("KPA", String.format("%.2f", getKPA()));
		config.setProperty("KDA", String.format("%.2f", getKDA()));
		config.setProperty("KIA", String.format("%.4f", getKIA()));
		config.setProperty("SPB", String.format("%d", getSPB()));
		config.setProperty("KPB", String.format("%.2f", getKPB()));
		config.setProperty("KDB", String.format("%.2f", getKDB()));
		config.setProperty("KIB", String.format("%.4f", getKIB()));
		config.setProperty("WAVEFORM", getWaveForm());
		config.setProperty("WAVEPERIOD", String.format("%d", getWavePeriod()));
		config.setProperty("WAVESTEPS", String.format("%d", getWaveSteps()));

		config.setProperty("CTRLPERIOD", String.format("%d", getCtrlPeriod()));
		config.setProperty("ACTVTHRESHOLD", String.format("%d", getActvThreshold()));
		config.setProperty("ACTVTIMEOUT", String.format("%d", getActvTimeout()));
		config.setProperty("SENSRVSD", String.format("%d", getSensReversed()?1:0));
		config.setProperty("SENSFILTER", String.format("%d", getSensFilter()));
		config.setProperty("COILFILTER", String.format("%d", getCoilFilter()));
		config.setProperty("COILMIN", String.format("%d", getCoilMin()));
		config.setProperty("COILMID", String.format("%d", getCoilMid()));
		config.setProperty("COILMAX", String.format("%d", getCoilMax()));

		config.setProperty("COILAXISMIN", String.format("%d",getCoilAxisMin()));
		config.setProperty("COILAXISMAX", String.format("%d",getCoilAxisMax()));
		
		config.setProperty("SENSAXISMIN", String.format("%d",getSensAxisMin()));
		config.setProperty("SENSAXISMAX", String.format("%d",getSensAxisMax()));
		
		FileOutputStream out=null;
		try {
			out=new FileOutputStream(file); 
			config.store(out, "");
			this.configFile = file;
			setModified(false);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(out!=null) {
				try { out.close(); 
				} catch(Throwable th) {
				};
			}
		}

		return true;
	}

	private boolean doOpen(File file) {
		Properties config = new Properties();
		try {
			config.load(new FileInputStream(file));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		String version = config.getProperty("VERSION");
		// TODO: Check version !!

		spSpinnerA.setValue(getInteger(config, "SPA", 0));
		kpSpinnerA.setValue(getDouble(config, "KPA", 0));
		kdSpinnerA.setValue(getDouble(config, "KDA", 0));
		kiSpinnerA.setValue(getDouble(config, "KIA", 0));

		spSpinnerB.setValue(getInteger(config, "SPB", 0));
		kpSpinnerB.setValue(getDouble(config, "KPB", 0));
		kdSpinnerB.setValue(getDouble(config, "KDB", 0));
		kiSpinnerB.setValue(getDouble(config, "KIB", 0));

		waveFormCombo.setSelectedItem(getString(config, "WAVEFORM", "Sin"));
		wavePeriodSpinner.setValue(getInteger(config, "WAVEPERIOD", 3000));
		waveStepsSpinner.setValue(getInteger(config, "WAVESTEPS", 100));

		ctrlPeriodSpinner.setValue(getInteger(config, "CTRLPERIOD", 1));
		actvThresholdSpinner.setValue(getInteger(config, "ACTVTHRESHOLD", 500));
		actvTimeoutSpinner.setValue(getInteger(config, "ACTVTIMEOUT", 2000));

		sensorReversedCheckBox.setSelected(getInteger(config, "SENSRVSD", 0)!=0?true:false);
		sensFilterSpinner.setValue(getInteger(config, "SENSFILTER", 2));
		coilFilterSpinner.setValue(getInteger(config, "COILFILTER", 2));

		coilMinSpinner.setValue(getInteger(config, "COILMIN", 0));
		coilMidSpinner.setValue(getInteger(config, "COILMID", 127));
		coilMaxSpinner.setValue(getInteger(config, "COILMAX", 255));
		
		coilAxisMinSpinner.setValue(getInteger(config, "COILAXISMIN", 0));
		coilAxisMaxSpinner.setValue(getInteger(config, "COILAXISMAX", 100));

		sensorAxisMinSpinner.setValue(getInteger(config, "SENSAXISMIN", 0));
		sensorAxisMaxSpinner.setValue(getInteger(config, "SENSAXISMAX", 1024));

		return true;
	}

	private String getString(Properties props, String key, String defaultValue) {
		String res = props.getProperty(key);
		return res == null ? defaultValue : res;
	}

	private int getInteger(Properties props, String key, int defaultValue) {
		try {
			return Integer.parseInt(props.getProperty(key));
		} catch (Throwable ex) {
			return defaultValue;
		}
	}

	private double getDouble(Properties props, String key, double defaultValue) {
		try {
			return Double.parseDouble(props.getProperty(key));
		} catch (Throwable ex) {
			return defaultValue;
		}
	}

	private void doExit() {
		if (modified) {
			int value = JOptionPane.showConfirmDialog(frame, "Discard changes?.", "Configuration has been modified",
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
			if (value != JOptionPane.YES_OPTION) {
				return;
			}
		}
		
		saveConfig();
		frame.dispose();
		System.exit(0);
	}

	@SuppressWarnings("unchecked")
	private void refreshPorts() {
		serialPortCombo.removeAllItems();
		String[] ports = SerialPortList.getPortNames();
		for (String p : ports) {
			serialPortCombo.addItem(p);
		}
		if (ports.length > 0) {
			serialPortCombo.setSelectedIndex(0);
			tglbtnConnect.setEnabled(true);
		} else {
			tglbtnConnect.setEnabled(false);
		}
	}

	private void generatorStart() {

		String type = getWaveForm();
		int periodTicks;

		if ("Square".equalsIgnoreCase(type)) {
			task = new SquareWaveTask(getSPA(), getKPA(), getKIA(), getKDA(), getSPB(), getKPB(), getKIB(), getKDB());
			periodTicks = getWavePeriod();

		} else if ("Triangle".equalsIgnoreCase(type)) {
			task = new TriangleWaveTask(getSPA(), getKPA(), getKIA(), getKDA(), getSPB(), getKPB(), getKIB(), getKDB(),
					getWaveSteps());
			periodTicks = getWavePeriod() / getWaveSteps();

		} else if ("Sin".equalsIgnoreCase(type)) {
			task = new SinWaveTask(getSPA(), getKPA(), getKIA(), getKDA(), getSPB(), getKPB(), getKIB(), getKDB(),
					getWaveSteps());
			periodTicks = getWavePeriod() / getWaveSteps();
		} else if ("Pulse".equalsIgnoreCase(type)) {
			task = new PulseWaveTask(getSPA(), getKPA(), getKIA(), getKDA(), getSPB(), getKPB(), getKIB(), getKDB(),
					getWaveSteps());
			periodTicks = getWavePeriod() / getWaveSteps();
		} else {
			return;
		}
		btnSendA.setEnabled(false);
		btnSendB.setEnabled(false);
		btnReadA.setEnabled(false);
		btnReadB.setEnabled(false);
		btnGenerate.setText("Stop");
		timer.scheduleAtFixedRate(task, 200, periodTicks);
	}

	private void generatorStop() {
		task.cancel();
		timer.purge();
		task = null;
		btnGenerate.setText("Start");
		btnSendA.setEnabled(true);
		btnSendB.setEnabled(true);
		btnReadA.setEnabled(true);
		btnReadB.setEnabled(true);
		query();
	}

	private void query() {
		sendCmd("$$\n");
	}

	private void report(long period) {
		sendCmd(String.format("?%d\n", period));
	}

	private void sendParameters(double setPoint, double kp, double ki, double kd) {
		sendCmd(String.format("!%d,%.2f,%.4f,%.2f\n", (long) setPoint, kp, ki, kd));
	}

	private void sendSave() {
		sendCmd("$<\n");
	}

	private void sendRestore() {
		sendCmd("$>\n");
	}

	private void sendCtrlPeriod(int period) {
		sendCmd(String.format("$PERIOD=%d\n", period));
	}

	private void sendThreshold(int threshold) {
		sendCmd(String.format("$THRSHLD=%d\n", threshold));
	}

	private void sendCtrlTimeout(int timeout) {
		sendCmd(String.format("$TIMEOUT=%d\n", timeout));
	}

	private void sendSensReversed(boolean reversed ) {
		sendCmd(String.format("$SENSRVSD=%d\n", reversed?1:0));
	}

	private void sendSensFilter(int filter) {
		sendCmd(String.format("$SENSFLTR=%d\n", filter));
	}

	private void sendCoilFilter(int filter) {
		sendCmd(String.format("$COILFLTR=%d\n", filter));
	}

	private void sendCoilMin(int min) {
		sendCmd(String.format("$COILMIN=%d\n", min*255/100));
	}

	private void sendCoilMid(int mid) {
		sendCmd(String.format("$COILMID=%d\n", mid*255/100));
	}

	private void sendCoilMax(int max) {
		sendCmd(String.format("$COILMAX=%d\n", max*255/100));
	}

	ExecutorService executorService = Executors.newSingleThreadExecutor();
	private JCheckBoxMenuItem chckbxmntmLoadPidA;
	private JCheckBoxMenuItem chckbxmntmLoadPidB;
	private JCheckBoxMenuItem chckbxmntmLoadSettingsUpon;
	private JLabel lblSensorReversed;
	private JCheckBox sensorReversedChecked;
	private JLabel lblSensorReversed_1;
	private JCheckBox sensorReversedCheckBox;
	private JMenuItem mntmSaveAsDefaults;
	private JSeparator separator;
	private JMenuItem mntmRestoreDefaults;
	
	private synchronized void sendCmd(String cmd) {
		if (serialPort == null || !serialPort.isOpened())
			return;
		System.out.print(cmd);
        try {
			serialPort.writeString(cmd);
		} catch (SerialPortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			doDisconnect();
		}
		
	}
	
	private void doDisconnect() {
		if(SwingUtilities.isEventDispatchThread()) {
			tglbtnConnect.setSelected(false);
		} else {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					tglbtnConnect.setSelected(false);
				}
			});			
		}
		
		
	}
	
	private int getCtrlPeriod() {
		return ((Number) ctrlPeriodSpinner.getValue()).intValue();
	}

	private int getActvTimeout() {
		return ((Number) actvTimeoutSpinner.getValue()).intValue();
	}

	private boolean getSensReversed() {
		return sensorReversedCheckBox.isSelected();
	}

	private int getSensFilter() {
		return ((Number) sensFilterSpinner.getValue()).intValue();
	}

	private int getCoilFilter() {
		return ((Number) coilFilterSpinner.getValue()).intValue();
	}

	private int getCoilMin() {
		return ((Number) coilMinSpinner.getValue()).intValue();
	}

	private int getCoilMid() {
		return ((Number) coilMidSpinner.getValue()).intValue();
	}

	private int getCoilMax() {
		return ((Number) coilMaxSpinner.getValue()).intValue();
	}

	private int getCoilAxisMin() {
		return ((Number) coilAxisMinSpinner.getValue()).intValue();
	}
	
	private int getCoilAxisMax() {
		return ((Number) coilAxisMaxSpinner.getValue()).intValue();
	}
	
	private int getSensAxisMin() {
		return ((Number) sensorAxisMinSpinner.getValue()).intValue();
	}
	
	private int getSensAxisMax() {
		return ((Number) sensorAxisMaxSpinner.getValue()).intValue();
	}
	
	private int getActvThreshold() {
		return ((Number) actvThresholdSpinner.getValue()).intValue();
	}

	private int getSP() {
		return Integer.parseInt(spTextField.getText());
	}

	private double getKP() {
		return Integer.parseInt(spTextField.getText());
	}

	private double getKI() {
		return ((Number) kiSpinnerA.getValue()).doubleValue();
	}

	private double getKD() {
		return ((Number) kdSpinnerA.getValue()).doubleValue();
	}

	private int getSPA() {
		return ((Number) spSpinnerA.getValue()).intValue();
	}

	private double getKPA() {
		return ((Number) kpSpinnerA.getValue()).doubleValue();
	}

	private double getKIA() {
		return ((Number) kiSpinnerA.getValue()).doubleValue();
	}

	private double getKDA() {
		return ((Number) kdSpinnerA.getValue()).doubleValue();
	}

	private int getSPB() {
		return ((Number) spSpinnerB.getValue()).intValue();
	}

	private double getKPB() {
		return ((Number) kpSpinnerB.getValue()).doubleValue();
	}

	private double getKIB() {
		return ((Number) kiSpinnerB.getValue()).doubleValue();
	}

	private double getKDB() {
		return ((Number) kdSpinnerB.getValue()).doubleValue();
	}

	private String getWaveForm() {
		return (String) waveFormCombo.getSelectedItem();
	}

	private int getWavePeriod() {
		return ((Number) wavePeriodSpinner.getValue()).intValue();
	}

	private int getWaveSteps() {
		return ((Number) waveStepsSpinner.getValue()).intValue();
	}

	private void updateSetPoint(final int sp) {
		final boolean _readA = readA;
		final boolean _readB = readB;
		spValue= sp;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				spTextField.setText(String.format("%d", sp));

				if (_readA) {
					spSpinnerA.setValue(sp);
				}
				if (_readB) {
					spSpinnerB.setValue(sp);
				}
			}
		});
	}

	private void updateKP(final float kp) {
		final boolean _readA = readA;
		final boolean _readB = readB;
		kpValue= kp;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				kpTextField.setText(String.format("%.2f", kp));

				if (_readA) {
					kpSpinnerA.setValue(kp);
				}
				if (_readB) {
					kpSpinnerB.setValue(kp);
				}
			}
		});
	}

	private void updateKD(final float kd) {
		final boolean _readA = readA;
		final boolean _readB = readB;
		kdValue= kd;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				kdTextField.setText(String.format("%.2f", kd));

				if (_readA) {
					kdSpinnerA.setValue(kd);
				}
				if (_readB) {
					kdSpinnerB.setValue(kd);
				}
			}
		});
	}

	private void updateKI(final float ki) {
		final boolean _readA = readA;
		final boolean _readB = readB;
		kiValue= ki;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				kiTextField.setText(String.format("%.4f", ki));

				if (_readA) {
					kiSpinnerA.setValue(ki);
				}
				if (_readB) {
					kiSpinnerB.setValue(ki);
				}
			}
		});
	}

	private void updatePeriod(final int period) {
		final boolean _readSettings = readSettings;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				ctrlPeriodTextField.setText(String.format("%d", period));
				if (_readSettings) {
					ctrlPeriodSpinner.setValue(period);
				}
			}
		});
	}

	private void updateThreshold(final int threshold) {
		final boolean _readSettings = readSettings;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				thresholdTextField.setText(String.format("%d", threshold));
				if (_readSettings) {
					actvThresholdSpinner.setValue(threshold);
				}
			}
		});
	}

	private void updateSensReversed(final boolean inverted) {
		final boolean _readSettings = readSettings;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				sensorReversedChecked.setSelected(inverted);
				if (_readSettings) {
					sensorReversedCheckBox.setSelected(inverted);
				}
			}
		});
	}

	private void updateSensFilter(final int filter) {
		final boolean _readSettings = readSettings;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				sensFilterTextField.setText(String.format("%d", filter));
				if (_readSettings) {
					sensFilterSpinner.setValue(filter);
				}
			}
		});
	}

	private void updateCoilFilter(final int filter) {
		final boolean _readSettings = readSettings;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				coilFilterTextField.setText(String.format("%d", filter));
				if (_readSettings) {
					coilFilterSpinner.setValue(filter);
				}
			}
		});
	}

	private void updateTimeout(final int timeout) {
		final boolean _readSettings = readSettings;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				ctrlTimeoutTextField.setText(String.format("%d", timeout));
				if (_readSettings) {
					actvTimeoutSpinner.setValue(timeout);
				}
			}
		});
	}

	private void updateCoilMin(final int min) {
		final boolean _readSettings = readSettings;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				coilMinTextField.setText(String.format("%d", min));
				if (_readSettings) {
					coilMinSpinner.setValue(min);
				}
			}
		});
	}

	private void updateCoilMid(final int mid) {
		final boolean _readSettings = readSettings;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				coilMidTextField.setText(String.format("%d", mid));
				if (_readSettings) {
					coilMidSpinner.setValue(mid);
				}
			}
		});
	}

	private void updateCoilMax(final int max) {
		final boolean _readSettings = readSettings;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				coilMaxTextField.setText(String.format("%d", max));
				if (_readSettings) {
					coilMaxSpinner.setValue(max);
				}
			}
		});
	}

	@Override
	public void serialEvent(SerialPortEvent evt) {
		if (!evt.isRXCHAR())
			return;
		lastReportDate= System.currentTimeMillis();
		try {
			String report;
			String[] parts;
			byte[] in = serialPort.readBytes();
			if (in == null)
				return;
			// System.out.print(new String(in));
			for (int t = 0; t < in.length; ++t) {
				int c = in[t] & 0xFF;
				if (c == '\n') {
					report = new String(buffer, 0, received);
					switch ((int) (buffer[0]) & 0xFF) {
					case '>':
						if (!connected)
							break;
						report = new String(buffer, 0, received);
						if (report.startsWith(">IDLE") || report.startsWith(">ACTV")) {
							parts = report.split(",");
							int l = parts.length;
							sensorData[0] = l > 1 ? Float.parseFloat(parts[1]) : 0;
							sensorData[1] = l > 2 ? Float.parseFloat(parts[2]) : 0;
							pwmData[0] = l > 3 ? (Float.parseFloat(parts[3])/2.55f) : 0;
							synchronized (sensorDataset) {
								sensorDataset.advanceTime();
								pwmDataset.advanceTime();
								sensorDataset.appendData(sensorData);
								pwmDataset.appendData(pwmData);
							}
						} else {
							System.out.println(report);
							if (">OK".equals(report)) {
								readA = readB = readSettings = false;
							} else if (report.startsWith(">SETTINGS,")) {
								query();
							}
						}
						break;
					case '$':
						parts = report.split("=");
						if ("$SP".equals(parts[0])) {
							int value = Integer.parseInt(parts[1]);
							updateSetPoint(value);
						} else if ("$KP".equals(parts[0])) {
							float value = Float.parseFloat(parts[1]);
							updateKP(value);
						} else if ("$KI".equals(parts[0])) {
							float value = Float.parseFloat(parts[1]);
							updateKI(value);
						} else if ("$KD".equals(parts[0])) {
							float value = Float.parseFloat(parts[1]);
							updateKD(value);
						} else if ("$SENSRVSD".equals(parts[0])) {
							boolean value = Integer.parseInt(parts[1])!=0;
							updateSensReversed(value);
						} else if ("$SENSFLTR".equals(parts[0])) {
							int value = Integer.parseInt(parts[1]);
							updateSensFilter(value);
						} else if ("$COILFLTR".equals(parts[0])) {
							int value = Integer.parseInt(parts[1]);
							updateCoilFilter(value);
						} else if ("$PERIOD".equals(parts[0])) {
							int value = Integer.parseInt(parts[1]);
							updatePeriod(value);
						} else if ("$THRSHLD".equals(parts[0])) {
							int value = Integer.parseInt(parts[1]);
							updateThreshold(value);
						} else if ("$TIMEOUT".equals(parts[0])) {
							int value = Integer.parseInt(parts[1]);
							updateTimeout(value);
						} else if ("$COILMIN".equals(parts[0])) {
							int value = Integer.parseInt(parts[1]);
							updateCoilMin(100*value/255);
						} else if ("$COILMID".equals(parts[0])) {
							int value = Integer.parseInt(parts[1]);
							updateCoilMid(100*value/255);
						} else if ("$COILMAX".equals(parts[0])) {
							int value = Integer.parseInt(parts[1]);
							updateCoilMax(100*value/255);
						} else {
							System.out.println(report);
						}
						break;
					default:
						System.out.println(report);

					}
					buffer[0] = 0;
					received = 0;
				} else if (received < 200) {
					if (c != '\r')
						buffer[received++] = (byte) (c);
				}
			}
		} catch (SerialPortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	class SquareWaveTask extends TimerTask {
		private double spA;
		private double kpA;
		private double kiA;
		private double kdA;
		private double spB;
		private double kpB;
		private double kiB;
		private double kdB;
		private boolean sendA;

		public SquareWaveTask(double spA, double kpA, double kiA, double kdA, double spB, double kpB, double kiB,
				double kdB) {
			this.spA = spA;
			this.kpA = kpA;
			this.kiA = kiA;
			this.kdA = kdA;
			this.spB = spB;
			this.kpB = kpB;
			this.kiB = kiB;
			this.kdB = kdB;
			sendA = true;
		}

		@Override
		public void run() {
			if (sendA) {
				sendParameters(spA, kpA, kiA, kdA);
			} else {
				sendParameters(spB, kpB, kiB, kdB);
			}
			sendA = !sendA;
		}

	}

	class TriangleWaveTask extends TimerTask {
		private double spA;
		private double kpA;
		private double kiA;
		private double kdA;
		private int steps;

		private double dsp;
		private double dkp;
		private double dki;
		private double dkd;

		private int inc;

		private int currentStep;

		public TriangleWaveTask(double spA, double kpA, double kiA, double kdA, double spB, double kpB, double kiB,
				double kdB, int steps) {
			this.spA = spA;
			this.kpA = kpA;
			this.kiA = kiA;
			this.kdA = kdA;
			this.steps = steps > 1 ? steps : 1;

			this.dsp = (spB - spA) / this.steps;
			this.dkp = (kpB - kpA) / this.steps;
			this.dki = (kiB - kiA) / this.steps;
			this.dkd = (kdB - kdA) / this.steps;

			this.steps = steps;

			inc = -1;
			currentStep = 1;
		}

		@Override
		public void run() {
			currentStep += inc;
			if (currentStep == 0)
				inc = 1;
			if (currentStep == steps)
				inc = -1;

			sendParameters(spA + currentStep * dsp, kpA + currentStep * dkp, kiA + currentStep * dki,
					kdA + currentStep * dkd);
		}

	}

	class SinWaveTask extends TimerTask {
		private double spA;
		private double kpA;
		private double kiA;
		private double kdA;
		private int steps;

		private double dsp;
		private double dkp;
		private double dki;
		private double dkd;

		
		
		private double angle;
		private double da;

		public SinWaveTask(double spA, double kpA, double kiA, double kdA, double spB, double kpB, double kiB,
				double kdB, int steps) {
			this.spA = spA;
			this.kpA = kpA;
			this.kiA = kiA;
			this.kdA = kdA;
			this.steps = steps > 1 ? steps : 1;

			this.dsp = (spB - spA);
			this.dkp = (kpB - kpA);
			this.dki = (kiB - kiA);
			this.dkd = (kdB - kdA);

			this.steps = steps > 1 ? steps : 1;

			da = 2 * Math.PI / this.steps;
			angle = -Math.PI/2 - da;
		}

		@Override
		public void run() {
			angle = (angle + da) % (2 * Math.PI);
			double sin = (1 + Math.sin(angle)) / 2;
			sendParameters(spA + sin * dsp, kpA + sin * dkp, kiA + sin * dki, kdA + sin * dkd);
		}

	}
	class PulseWaveTask extends TimerTask {
		static final private int INIT=0;
		static final private int PULSE=1;
		static final private int FLAT=2;
		
		private double sp;
		private double kp;
		private double ki;
		private double kd;

		private double spA;
		private double kpA;
		private double kiA;
		private double kdA;
		private double spB;
		private double kpB;
		private double kiB;
		private double kdB;
		private int steps;

		private double dsp;
		private double dkp;
		private double dki;
		private double dkd;

		
		private double angle;
		private double da;

		private int state;
		private int cpt;
		
		public PulseWaveTask(double spA, double kpA, double kiA, double kdA, double spB, double kpB, double kiB,
				double kdB, int steps) {
			this.spA = spA;
			this.kpA = kpA;
			this.kiA = kiA;
			this.kdA = kdA;
			this.spB = spB;
			this.kpB = kpB;
			this.kiB = kiB;
			this.kdB = kdB;
			this.steps = steps > 1 ? steps : 1;
			da = 2 * Math.PI / this.steps;
			angle = -Math.PI/2 - da;

			state=INIT;

			this.sp= Main.this.spValue;
			this.kp= Main.this.kpValue;
			this.ki= Main.this.kiValue;
			this.kd= Main.this.kdValue;
			cpt=this.steps/2;

			this.dsp = ((sp - spA))/cpt;
			this.dkp = ((kp - kpA))/cpt;
			this.dki = ((ki - kiA))/cpt;
			this.dkd = ((kd - kdA))/cpt;
			
		}

		@Override
		public void run() {
			double v;
			
			switch(state) {
			case INIT:
				sp-= dsp;
				kp-= dkp;
				ki-= dki;
				kd-= dkd;
				--cpt;
				if(cpt<=0) {
					state=FLAT;
					cpt=steps;
					dsp = (spB - spA);
					dkp = (kpB - kpA);
					dki = (kiB - kiA);
					dkd = (kdB - kdA);
				}
				break;
			case PULSE:
				angle = (angle + da) % (2 * Math.PI);
				v = (1 + Math.sin(angle)) / 2 ;
				sp= spA+v*dsp;
				kp= kpA+v*dkp;
				ki= kiA+v*dki;
				kd= kdA+v*dkd;
				--cpt;
				if(cpt<=0) {
					cpt=2*steps;
					state=FLAT;
				}
				break;
			case FLAT:
				--cpt;
				if(cpt<=0) {
					state=PULSE;
					cpt=2*steps;
					angle = -Math.PI/2 - da;
				}
				break;
			}
			sendParameters(sp, kp, ki, kd);

		}
	}
}

@SuppressWarnings("serial")
class MyDynamicTimeSeriesCollection extends DynamicTimeSeriesCollection {
	public MyDynamicTimeSeriesCollection(int nSeries, int nMoments, RegularTimePeriod timeSample) {
		super(nSeries, nMoments, timeSample);
		if (timeSample instanceof MultipleOfMillisecond) {
			this.pointsInTime = new MultipleOfMillisecond[nMoments];
		} else if (timeSample instanceof Millisecond) {
			this.pointsInTime = new Millisecond[nMoments];
		}
	}
}

class MultipleOfMillisecond extends Millisecond {

	private static final long serialVersionUID = 1L;
	private int periodMs = 100;

	public MultipleOfMillisecond(int periodMs) {
		super();
		this.periodMs = periodMs;
	}

	public MultipleOfMillisecond(int periodMs, int millisecond, Second second) {
		super(millisecond, second);
		this.periodMs = periodMs;
	}

	@Override
	public RegularTimePeriod next() {

		RegularTimePeriod result = null;
		if (getMillisecond() + periodMs <= LAST_MILLISECOND_IN_SECOND) {
			result = new MultipleOfMillisecond(periodMs, (int) (getMillisecond() + periodMs), getSecond());
		} else {
			Second next = (Second) getSecond().next();
			if (next != null) {
				result = new MultipleOfMillisecond(periodMs,
						(int) (getMillisecond() + periodMs - LAST_MILLISECOND_IN_SECOND - 1), next);
			}
		}
		return result;

	}

	@Override
	public RegularTimePeriod previous() {

		RegularTimePeriod result = null;
		if (getMillisecond() - periodMs >= FIRST_MILLISECOND_IN_SECOND) {
			result = new MultipleOfMillisecond(periodMs, (int) getMillisecond() - periodMs, getSecond());
		} else {
			Second previous = (Second) getSecond().previous();
			if (previous != null) {
				result = new MultipleOfMillisecond(periodMs,
						(int) (getMillisecond() - periodMs + LAST_MILLISECOND_IN_SECOND + 1), previous);
			}
		}
		return result;

	}

}