![levitatorhost-300x300.png](/doc/img/levitatorhost-300x300.png)

***
# Arduino Magnetic Levitator Host controller
***


### Release v1.6 available

Fix crash on windows due to the bug in JSSC 2.8.0 on windows.

* Windows binary: [levitatorhost-1.6.exe](https://bitbucket.org/rixnco/levitatorhost/downloads/levitatorhost-1.6.exe)
* Executable jar file: [levitatorhost-1.6.jar](https://bitbucket.org/rixnco/levitatorhost/downloads/levitatorhost-1.6.jar)

* All downloads: [downloads](https://bitbucket.org/rixnco/levitatorhost/downloads/)

![levitatorhost-1.0.png](/doc/img/levitatorhost-1.0.png)

### Arduino Levitator

Find the related arduino project here: [Levitator](https://bitbucket.org/rixnco/levitator)

### Build a Levitator

Find all the necessary instructions to build a complete Levitator her: [Thing](https://goo.gl/UQwSkJ)

![christmastree.png](/doc/img/christmastree.png)
